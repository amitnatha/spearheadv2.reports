USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateProduct]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateProduct]
GO


CREATE PROCEDURE [Reports].[RptAffiliateProduct]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @StartDate = DATEADD(MONTH,-11,@StartDate)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+12,0)))


--------------- Insert Leads Received ----------------
SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived, 
	   dp.Name AS ProductName
INTO 
	   #AffiliateProductCnt
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   dp.Name



------------- Insert Pivot records into temp table -------------------------
SELECT 
	  *
INTO  
	  #AffiliateProductPivot
FROM (
      SELECT 
			LeadYear,
			DATENAME(MONTH,CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)) AS LeadMonth, 
			LeadsReceived,
			ProductName
	  FROM
		    #AffiliateProductCnt WITH (NOLOCK)
) AS dervtbl
PIVOT
(
 SUM(LeadsReceived)
 FOR LeadMonth IN (january, february, march, april, may, june, july, august, september, october, november, december)
)AS piv;



--------------- Insert NULL records in Pivot Table that does not have a record for a particular year -----------------
DECLARE @MinYear NVARCHAR (10),
@MaxYear NVARCHAR (10)

SELECT @MinYear = (Select MIN(LeadYear) FROM #AffiliateProductPivot WITH (NOLOCK))
SELECT @MaxYear = (Select MAX(LeadYear) FROM #AffiliateProductPivot WITH (NOLOCK))

SELECT @MinYear = CAST(@MinYear + '01' + '01' AS DATE)
SELECT @MaxYear = CAST(@MaxYear + '01' + '01' AS DATE)

;WITH CTE AS
(
SELECT
	  COUNT(*) OVER (PARTITION BY ProductName) AS TotalRecords,
	  ProductName,
	  LeadYear
FROM
	  #AffiliateProductPivot WITH (NOLOCK) 
GROUP BY
	  ProductName, 
	  leadYear
)
INSERT INTO	#AffiliateProductPivot
(
 LeadYear,
 ProductName
)
SELECT 
	  NewYear = CASE 
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MaxYear) >=1 
				THEN DATEPART(YEAR,@MaxYear)
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MinYear) <1 
				THEN DATEPART(YEAR,@MinYear)  
				END,
	  CTE.ProductName
FROM
	  #AffiliateProductPivot piv WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON cte.ProductName = piv.ProductName
WHERE
	  CTE.TotalRecords < 2




------------------------ Insert data into Final Table using Unpivot ------------------------
SELECT
	  app.LeadYear, 
	  app.ProductName, 
	  MONTH(dervtbl.LeadMonth + ' 1 2010') AS LeadMonth,
	  Leads = 
			 CASE dervtbl.LeadMonth
				  when 'January' then app.January
				  when 'February' then app.February
				  when 'March' then app.March
				  when 'April' then app.April
				  when 'May' then app.May
				  when 'June' then app.June
				  when 'July' then app.July
				  when 'August' then app.August
				  when 'September' then app.September
				  when 'October' then app.October
				  when 'November' then app.November
				  when 'December' then app.December
	        END
INTO #RptAffiliateProductTbl
FROM (
	  SELECT LeadYear, ProductName, january, february, march, april, may, june, july, august, september, october, november, december
	  FROM #AffiliateProductPivot app WITH (NOLOCK)
  ) app
CROSS JOIN (
			  select 'January' union all
			  select 'February' union all
			  select 'March' union all
			  select 'April' union all
			  select 'May' union all
			  select 'June' union all
			  select 'July' union all
			  select 'August' union all
			  select 'September' union all
			  select 'October' union all
			  select 'November' union all
			  select 'December'
			) dervtbl (LeadMonth)


--------- Select Final Records for the report ------------------
SELECT
	  LeadYear,
	  LeadMonth,
	  ProductName,
	  ISNULL(Leads, 0) AS LeadsReceived
FROM
	  #RptAffiliateProductTbl WITH (NOLOCK)
WHERE 
	  CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) >= @StartDate
	  AND CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) <= @EndDate
ORDER BY 
	  ProductName ASC

RETURN

---------- Drop Affiliate Product Cnt Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateProductCnt') AND type in (N'U'))
DROP TABLE #AffiliateProductCnt

---------- Drop Affiliate Product Pivot Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateProductPivot') AND type in (N'U'))
DROP TABLE #AffiliateProductPivot

---------- Drop RptAffiliate Product Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#RptAffiliateProductTbl') AND type in (N'U'))
DROP TABLE #RptAffiliateProductTbl

END