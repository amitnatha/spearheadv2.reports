USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateEarnings]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateEarnings]
GO


CREATE PROCEDURE [Reports].[RptAffiliateEarnings]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE,
		@ReportName NVARCHAR (50)


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @StartDate = DATEADD(MONTH,-11,@StartDate)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+12,0)))
SET @ReportName = 'RptClientEarnings'

BEGIN TRY

--------------- Insert Earnings --------------------------
SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   da.Name AS AffiliateName,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
INTO 
	   #AffiliateEarningsCnt
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dau.Userid IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   --AND dld.PaymentStructureId = 1
	   and dld.IsDuplicate = 0
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   da.Name,
	   ActualEarnings


------------- Insert Pivot records into temp table -------------------------
SELECT 
	  *
INTO  
	  #AffiliateEarningsPivot
FROM (
      SELECT 
			LeadYear,
			DATENAME(MONTH,CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)) AS LeadMonth, 
			Earnings,
			AffiliateName,
			IsActualEarnings
	  FROM
		    #AffiliateEarningsCnt WITH (NOLOCK)
) AS dervtbl
PIVOT
(
 SUM(Earnings)
 FOR LeadMonth IN (january, february, march, april, may, june, july, august, september, october, november, december)
)AS piv;



--------------- Insert NULL records in Pivot Table that does not have a record for a particular year -----------------
DECLARE @MinYear NVARCHAR (10),
@MaxYear NVARCHAR (10)

SELECT @MinYear = (Select MIN(LeadYear) FROM #AffiliateEarningsPivot WITH (NOLOCK))
SELECT @MaxYear = (Select MAX(LeadYear) FROM #AffiliateEarningsPivot WITH (NOLOCK))

SELECT @MinYear = CAST(@MinYear + '01' + '01' AS DATE)
SELECT @MaxYear = CAST(@MaxYear + '01' + '01' AS DATE)

;WITH CTE AS
(
SELECT
	  COUNT(*) OVER (PARTITION BY AffiliateName) AS TotalRecords,
	  AffiliateName,
	  LeadYear,
	  IsActualEarnings
FROM
	  #AffiliateEarningsPivot WITH (NOLOCK) 
GROUP BY
	  AffiliateName, 
	  leadYear,
	  IsActualEarnings
)
INSERT INTO	#AffiliateEarningsPivot
(
 LeadYear,
 AffiliateName,
 IsActualEarnings
)
SELECT 
	  NewYear = CASE 
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MaxYear) >=1 
				THEN DATEPART(YEAR,@MaxYear)
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MinYear) <1 
				THEN DATEPART(YEAR,@MinYear)  
				END,
	  CTE.AffiliateName,
	  0 AS IsActualEarnings
FROM
	  #AffiliateEarningsPivot piv WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON CTE.AffiliateName = piv.AffiliateName
WHERE
	  CTE.TotalRecords < 2


	  
------------------------ Insert data into Final Table using Unpivot ------------------------
SELECT
	  aep.LeadYear, 
	  aep.AffiliateName, 
	  MONTH(dervtbl.LeadMonth + ' 1 2010') AS LeadMonth,
	  Earnings = 
					 CASE dervtbl.LeadMonth
						  when 'January' then aep.January
						  when 'February' then aep.February
						  when 'March' then aep.March
						  when 'April' then aep.April
						  when 'May' then aep.May
						  when 'June' then aep.June
						  when 'July' then aep.July
						  when 'August' then aep.August
						  when 'September' then aep.September
						  when 'October' then aep.October
						  when 'November' then aep.November
						  when 'December' then aep.December
					END,
	  aep.IsActualEarnings
INTO #RptAffiliateEarningsTbl
FROM (
	  SELECT LeadYear, AffiliateName, january, february, march, april, may, june, july, august, september, october, november, december, IsActualEarnings
	  FROM #AffiliateEarningsPivot aep WITH (NOLOCK)
  ) aep
CROSS JOIN (
			  select 'January' union all
			  select 'February' union all
			  select 'March' union all
			  select 'April' union all
			  select 'May' union all
			  select 'June' union all
			  select 'July' union all
			  select 'August' union all
			  select 'September' union all
			  select 'October' union all
			  select 'November' union all
			  select 'December'
			) dervtbl (LeadMonth)


--------- Select Final Records for the report ------------------
SELECT
	  LeadYear,
	  LeadMonth,
	  AffiliateName,
	  ISNULL(Earnings, 0) AS Earnings,
	  IsActualEarnings
FROM
	  #RptAffiliateEarningsTbl WITH (NOLOCK)
WHERE 
	  CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) >= @StartDate
	  AND CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) <= @EndDate
ORDER BY 
	  AffiliateName ASC

RETURN


---------- Drop Affiliate Earnings Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateEarningsCnt') AND type in (N'U'))
DROP TABLE #AffiliateEarningsCnt

---------- Drop Affiliate Earnings Pivot Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateEarningsPivot') AND type in (N'U'))
DROP TABLE #AffiliateEarningsPivot

---------- Drop RptAffiliateEarnings Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#RptAffiliateEarningsTbl') AND type in (N'U'))
DROP TABLE #RptAffiliateEarningsTbl

END TRY 

----------------- Insert Into Exception Table if theres an error --------------------
BEGIN CATCH

INSERT INTO
	   DataMartStaging.dbo.SsrsErrorException
( 
 SpName,
 ReportName,
 TenantParameter,
 Parameter1,
 Parameter2,
 Parameter3,
 Parameter4,
 Parameter5,
 Parameter6,
 Parameter7,
 Parameter8,
 ErrorDescription,
 LineNumber,
 InsertDateTime
)
SELECT 
	  ERROR_PROCEDURE(),
	  @ReportName,
	  @TenantID,
	  'Product: ' + CONVERT(NVARCHAR(MAX), @Product, 110),
	  'AffiliateManager: ' + CONVERT(NVARCHAR(MAX), @AffiliateManager, 110),
	  'StartDate: ' + CONVERT(NVARCHAR(MAX), @StartDate, 110),
	  'EndDate: ' + CONVERT(NVARCHAR(MAX), @EndDate, 110),
	  'ParentAffiliate: ' + CONVERT(NVARCHAR(MAX), @ParentAffiliate, 110),
	  NULL AS Parameter6,
	  NULL AS Parameter7,
	  NULL AS Parameter8,
	  ERROR_MESSAGE(),
	  ERROR_LINE(),
	  GETDATE()

------ Force the SQL Job to fail. The job would be shown as success if it had failed after catching the error	    
RAISERROR('Please refer to the SsrsErrorException table for the full error message',16,1)
		   
END CATCH

END

