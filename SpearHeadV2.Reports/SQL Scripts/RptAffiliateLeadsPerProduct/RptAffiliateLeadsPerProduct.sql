USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateLeadsPerProduct]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateLeadsPerProduct]
GO


CREATE PROCEDURE [Reports].[RptAffiliateLeadsPerProduct]
@StartDate DATE,
@EndDate DATE,
@ParentAffiliate NVARCHAR(MAX),
@Affiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   ach.Code AS AffiliateShortCode,
	   ach.[Description] AS AffiliateShortCodeDescription,
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   NULL AS UniqueLeadsAccepted,
	   NULL AS UniqueSales
INTO 
	   #AffiliateLeadsPerProduct
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   AffiliateShortCode ach WITH (NOLOCK)
	   ON ach.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   ach.Code,
	   ach.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   ach.Code AS AffiliateShortCode,
	   dld.ProductId
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   AffiliateShortCode ach WITH (NOLOCK)
	   ON ach.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
 GROUP BY 
	   ach.Code,
	   ach.[Description],
	   dld.ProductId
) 
UPDATE 
	  #AffiliateLeadsPerProduct
SET 
	  UniqueLeadsAccepted = CTE.Cnt
FROM 
	  #AffiliateLeadsPerProduct alp WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON alp.ProductId = CTE.ProductId
	  AND alp.AffiliateShortCode = CTE.AffiliateShortCode



--------------- Insert Leads Accepted where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Accepted cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the accepted cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO #AffiliateLeadsPerProduct
SELECT
	   ach.Code AS AffiliateShortCode,
	   ach.[Description] AS AffiliateSortCodeDescription,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS UnuiqueLeadsAccepted, 
	   0 AS UnqueSales
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   AffiliateShortCode ach WITH (NOLOCK)
	   ON ach.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #AffiliateLeadsPerProduct alp WITH (NOLOCK)
	   ON alp.ProductId = dld.ProductId
	   AND alp.AffiliateShortCode = ach.Code
 WHERE 
	   dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND alp.UniqueLeadsAccepted IS NULL
 GROUP BY 
	   ach.Code,
	   ach.[Description],
	   dp.ProductId,
	   dp.Name



--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dl.LeadId) AS Cnt,
	   ach.Code AS AffiliateShortCode,
	   dld.ProductId
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   AffiliateShortCode ach WITH (NOLOCK)
	   ON ach.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
GROUP BY 
	   ach.Code,
	   ach.[Description],
	   dld.ProductId
) 
UPDATE 
	  #AffiliateLeadsPerProduct
SET 
	  UniqueSales = CTE.Cnt
FROM 
	  #AffiliateLeadsPerProduct alp WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON alp.ProductId = CTE.ProductId
	  AND alp.AffiliateShortCode = CTE.AffiliateShortCode



--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #AffiliateLeadsPerProduct
SELECT DISTINCT
	   ach.Code AS AffiliateShortCode,
	   ach.[Description] AS AffiliateSortCodeDescription,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold, 
	   0 AS UnqueSales
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   AffiliateShortCode ach WITH (NOLOCK)
	   ON ach.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #AffiliateLeadsPerProduct alp WITH (NOLOCK)
	   ON alp.ProductId = dp.ProductId
	   AND alp.AffiliateShortCode = ach.Code
 WHERE 
	   dld.IsSold = 1
	   AND alp.UniqueSales IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
 GROUP BY 
	   ach.Code,
	   ach.[Description],
	   dp.ProductId,
	   dp.Name


----------- Select Records for the Report ------------------
SELECT
	  AffiliateShortCode,
	  AffiliateShortCodeDescription,
	  Product,
	  ISNULL(LeadsReceived, 0) AS LeadsReceived,
	  ISNULL(UniqueLeadsAccepted, 0) AS UniqueLeadsAccepted,
	  ISNULL(UniqueSales, 0) AS UniqueSales
FROM
      #AffiliateLeadsPerProduct WITH (NOLOCK)
ORDER BY
	  AffiliateShortCode ASC

RETURN

---------- Drop #ClientProductOverviewByChannel Temp table -------------------
IF  OBJECT_ID (N'tempdb..#AffiliateLeadsPerProduct') IS NOT NULL
DROP TABLE #AffiliateLeadsPerProduct




END