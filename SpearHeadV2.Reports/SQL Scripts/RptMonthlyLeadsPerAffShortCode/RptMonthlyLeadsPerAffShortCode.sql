USE [DataMartV2]
GO
/****** Object:  StoredProcedure [Reports].[RptMonthlyLeadsPerAffShortCode]    Script Date: 2015-09-03 07:54:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptMonthlyLeadsPerAffShortCode]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptMonthlyLeadsPerAffShortCode]
GO


CREATE PROCEDURE [Reports].[RptMonthlyLeadsPerAffShortCode]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@ParentAffiliate NVARCHAR(MAX),
@Affiliate NVARCHAR(MAX),
@ShortCodeId NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@Client NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE,
		@SpName NVARCHAR (50),
		@ReportName NVARCHAR (50)

SET @ReportName = 'RptClientEarnings'


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-5,@StartDate) -- Set the start date to 6 months prior to the month selected

BEGIN TRY


----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   Seed = IDENTITY (INT, 1,1),
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   dc.ClientId,
	   dc.Name AS Client,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS UniqueLeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS UniqueLeadsAccepted,
	   NULL AS Sales,
	   0 AS UniqueSales,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS EPL,
	   NULL AS Rn,
	   0 AS IsActualEarnings
INTO 
	   #MonthlyLeadsPerAffShortCode
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))	   
	   AND dt.OldTenantId = @TenantID
	   --AND dc.Name NOT LIKE '%*** test%'
	   AND dl.IsTest = 0
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.Name,
	   dc.ClientId,
	   dp.ProductId


----- Set Rn --------
;WITH CTE AS
(
 SELECT 
	   Seed,
	   ROW_NUMBER() OVER (PARTITION BY AffiliateShortCode, LeadDate ORDER BY LeadDate ASC) AS Rn
 FROM 
	   #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
)
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Rn = CTE.Rn
FROM 
	  #MonthlyLeadsPerAffShortCode AS MLAS WITH (NOLOCK) 
INNER JOIN
	  CTE
	  ON CTE.Seed = MLAS.Seed

--------------- Set Unique Leads Received -------------------
;WITH CTE AS
(
 SELECT distinct
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode
 FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
LEFT OUTER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   --AND dc.Name IN (SELECT Value FROM dbo.FnSplit(@Client,','))	   
	   AND dt.OldTenantId = @TenantID
	   --AND dc.Name NOT LIKE '%*** test%'
	   AND dld.IsValid = 1
	   AND dl.IsTest = 0  
	   AND dld.IsReceived = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  UniqueLeadsReceived = CTE.Cnt
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.LeadDate = CTE.LeadDate
	  AND mlas.AffiliateShortCode = CTE.ShortCode
WHERE
	  mlas.Rn = 1


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode,
	   dp.ProductId,
	   dc.ClientId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dl.IsTest = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dp.ProductId,
	   dc.ClientId
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.LeadDate = CTE.LeadDate
	  AND mlas.AffiliateShortCode = CTE.ShortCode
	  AND mlas.ProductId = CTE.ProductId
	  AND mlas.ClientId = CTE.ClientId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #MonthlyLeadsPerAffShortCode
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   dc.ClientId,
	   dc.NAME AS Client,
	   0 AS LeadsReceived,
	   0 AS UniqueLeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,  
	   0 AS UniqueLeadsAccepted,
	   0 AS Sales,
	   0 AS UniqueSales,
	   0.00 AS Earnings,
	   0.00 AS ConversionRate,
	   0.00 AS EPL,
	   NULL AS Rn,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
	   ON mlas.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND mlas.AffiliateShortCode = dasc.Code
	   AND mlas.ProductId = dp.ProductId
	   AND mlas.ClientId = dc.ClientId
 WHERE 
	   dld.IsAccepted = 1
	   AND mlas.LeadsAccepted IS NULL
	   --AND dc.Name NOT LIKE '%*** test%'
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dl.IsTest = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name,
	   dc.ClientId,
	   dc.NAME


----- Set Rn --------
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET
	  Rn = NULL

;WITH CTE AS
(
 SELECT 
	   Seed,
	   ROW_NUMBER() OVER (PARTITION BY AffiliateShortCode, LeadDate ORDER BY LeadDate ASC) AS Rn
 FROM 
	   #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
)
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Rn = CTE.Rn
FROM 
	  #MonthlyLeadsPerAffShortCode AS MLAS WITH (NOLOCK) 
INNER JOIN
	  CTE
	  ON CTE.Seed = MLAS.Seed


--------------- Set Unique Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 INNER JOIN	 
	   #MonthlyLeadsPerAffShortCode AS DMLA WITH (NOLOCK)
	   ON DMLA.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND DMLA.AffiliateShortCode = dasc.Code
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   --AND CONVERT(Date,dld.SubmittedDateTime) >= @StartDate AND CONVERT(Date,dld.SubmittedDateTime) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dl.IsTest = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  UniqueLeadsAccepted = CTE.Cnt
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.LeadDate = CTE.LeadDate
	  AND mlas.AffiliateShortCode = CTE.ShortCode
WHERE
	  mlas.Rn = 1


--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SalesDate,
	   dasc.Code AS ShortCode,
	   dp.ProductId,
	   dc.ClientId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAccepted = 1 A lead might be accepted & sold on different day
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dl.IsTest = 0
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dp.ProductId,
	   dc.ClientId
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Sales = CTE.Cnt
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.LeadDate = CTE.SalesDate
	  AND mlas.AffiliateShortCode = CTE.ShortCode
	  AND mlas.ProductId = CTE.ProductId
	  AND mlas.ClientId = CTE.ClientId


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #MonthlyLeadsPerAffShortCode
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SoldDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   dc.ClientId,
	   dc.NAME AS Client,
	   0 AS LeadsReceived,
	   0 AS UniqueLeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS UniqueLeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS Sales,  
	   0 AS UniqueSales,
	   0.00 AS Earnings,
	   0.00 AS ConversionRate,
	   0.00 AS EPL,
	   NULL AS Rn,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
	   ON mlas.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND mlas.AffiliateShortCode = dasc.Code
	   AND mlas.ProductId = dp.ProductId
	   AND mlas.ClientId = dc.ClientId
 WHERE 
	   --dld.IsAccepted = 1 A lead might be accepted & sold on different day
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND mlas.Sales IS NULL
	   --AND dc.Name NOT LIKE '%*** test%'
	   AND dl.IsTest = 0
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name,
	   dc.ClientId,
	   dc.NAME


----- Set Rn --------
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET
	  Rn = NULL

;WITH CTE AS
(
 SELECT 
	   Seed,
	   ROW_NUMBER() OVER (PARTITION BY AffiliateShortCode, LeadDate ORDER BY LeadDate ASC) AS Rn
 FROM 
	   #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
)
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Rn = CTE.Rn
FROM 
	  #MonthlyLeadsPerAffShortCode AS MLAS WITH (NOLOCK) 
INNER JOIN
	  CTE
	  ON CTE.Seed = MLAS.Seed


--------------- Set Unique Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SalesDate,
	   dasc.Code AS ShortCode
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN	 
	   #MonthlyLeadsPerAffShortCode AS DMLA WITH (NOLOCK)
	   ON DMLA.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND DMLA.AffiliateShortCode = dasc.Code
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAccepted = 1 A lead might be sold & accepted on a different day
	   --AND CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dl.IsTest = 0
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  UniqueSales = CTE.Cnt
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.LeadDate = CTE.SalesDate
	  AND mlas.AffiliateShortCode = CTE.ShortCode
WHERE 
	  mlas.Rn = 1


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffiliateShortCode,
	   dp.ProductId,
	   dc.ClientId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dl.IsTest = 0
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dp.ProductId,
	   dc.ClientId,
	   ActualEarnings
)
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = mlas.LeadDate
	  AND CTE.AffiliateShortCode = mlas.AffiliateShortCode
	  AND CTE.ProductId = mlas.ProductId
	  AND CTE.ClientId = mlas.ClientId


--------------- Insert Earnings --------------------------
--- Insert lead income records where the date does not appear in the temp table ---

INSERT INTO 
	   #MonthlyLeadsPerAffShortCode
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   dc.ClientId,
	   dc.NAME AS Client,
	   0 AS LeadsReceived,
	   0 AS UniqueLeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS UniqueLeadsAccepted,
	   0 AS Sales,  
	   0 AS UniqueSales,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   0.00 AS ConversionRate,
	   0.00 AS EPL,
	   NULL AS Rn,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK)
	   ON mlas.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND mlas.AffiliateShortCode = dasc.Code
	   AND mlas.ProductId = dp.ProductId
	   AND mlas.ClientId = dc.ClientId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND (('selectall' IN (@Affiliate)) or da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,',')))
	   AND (('selectall' IN (@ShortCodeId)) or dasc.AffiliateShortCodeId IN (SELECT Value FROM dbo.FnSplit(@ShortCodeId,',')))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAccepted = 1
	   AND mlas.Earnings IS NULL
	   --AND dc.Name NOT LIKE '%*** test%'
	   AND dl.IsTest = 0
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name,
	   dc.ClientId,
	   dc.NAME,
	   ActualEarnings



--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   ClientId,
	   CASE WHEN LeadsAccepted = 0 THEN 0.00 
		    ELSE CONVERT(DECIMAL (5,3),CAST(SUM(Sales) AS DECIMAL) / CAST(SUM(LeadsAccepted) AS DECIMAL)) END AS ConversionCal
 FROM
	   #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   ClientId,
	   LeadsAccepted,
	   Sales
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #MonthlyLeadsPerAffShortCode mlas
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.ProductId = CTE.ProductId
	  AND mlas.ClientId = CTE.ClientId
	  AND mlas.AffiliateShortCode = CTE.AffiliateShortCode
	  AND mlas.LeadDate = CTE.LeadDate
WHERE 
	  CTE.ConversionCal IS NOT NULL


--------------- Update EPL -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   ClientId,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsAccepted) END AS EPL -- Set EPL to 0 if there were no leads sent for a particular day
 FROM
	   #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   ClientId,
	   LeadsAccepted
) 
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  EPL = CTE.EPL
FROM 
	  #MonthlyLeadsPerAffShortCode mlas WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON mlas.ProductId = CTE.ProductId
	  AND mlas.ClientId = CTE.ClientId
	  AND mlas.AffiliateShortCode = CTE.AffiliateShortCode
	  AND mlas.LeadDate = CTE.LeadDate

----------- Set NULL values to 0 -------------------
UPDATE 
	  #MonthlyLeadsPerAffShortCode
SET 
	  Sales = 0
WHERE
	  Sales IS NULL

--------------- Select records for the report -------------------
SELECT 
	   LeadDate,
	   AffiliateShortCode,
	   [description],
	   Product,
	   Client,
	   LeadsReceived,
	   UniqueLeadsReceived,
	   Sales,
	   UniqueSales,
	   LeadsAccepted,
	   UniqueLeadsAccepted,
	   Earnings,
	   ConversionRate,
	   EPL,
	   IsActualEarnings
FROM 
	  #MonthlyLeadsPerAffShortCode WITH (NOLOCK)
ORDER BY
	  YEAR(LeadDate) ASC, MONTH(LeadDate) ASC,
	  AffiliateShortCode ASC

RETURN

---------- Drop #MonthlyLeadsPerAffShortCode Temp table -------------------
IF  OBJECT_ID (N'tempdb..#MonthlyLeadsPerAffShortCode') IS NOT NULL
DROP TABLE #MonthlyLeadsPerAffShortCode


END TRY



----------------- Insert Into Exception Table if theres an error --------------------
BEGIN CATCH

INSERT INTO
	   DataMartStaging.dbo.SsrsErrorException
( 
 SpName,
 ReportName,
 TenantParameter,
 Parameter1,
 Parameter2,
 Parameter3,
 Parameter4,
 Parameter5,
 Parameter6,
 Parameter7,
 Parameter8,
 ErrorDescription,
 LineNumber,
 InsertDateTime
)
SELECT 
	  ERROR_PROCEDURE(),
	  @ReportName,
	  @TenantID,
	  'Product: ' + CONVERT(NVARCHAR(MAX), @Product, 110),
	  'Client: ' + CONVERT(NVARCHAR(MAX), @Client, 110),
	  'StartDate: ' + CONVERT(NVARCHAR(MAX), @StartDate, 110),
	  'EndDate: ' + CONVERT(NVARCHAR(MAX), @EndDate, 110),
	  'Affiliate: ' + CONVERT(NVARCHAR(MAX), @Affiliate, 110),
	  'ParentAffiliate: ' + CONVERT(NVARCHAR(MAX), @ParentAffiliate, 110),
	  'ShortCodeId: ' + CONVERT(NVARCHAR(MAX), @ShortCodeId, 110),
	  NULL AS Parameter8,
	  ERROR_MESSAGE(),
	  ERROR_LINE(),
	  GETDATE()

------ Force the SQL Job to fail. The job would be shown as success if it had failed after catching the error	    
RAISERROR('Please refer to the SsrsErrorException table for the full error message',16,1)
		   
END CATCH

END






