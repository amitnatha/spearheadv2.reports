USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptOperationsProfitOverview]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptOperationsProfitOverview]
GO


CREATE PROCEDURE [Reports].[RptOperationsProfitOverview]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@ParentAffiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-11,@StartDate) -- Set the start date to 6 months prior to the month selected



--------------- Insert Earnings --------------------------
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   SUM(ISNULL(dld.ProjectedEarnings,0)) AS Earnings,
	   --CONVERT(DECIMAL(10,2), 0) AS SaleIncome,
	   --CONVERT(DECIMAL(10,2), 0) AS CancelledIncome,
	   CONVERT(DECIMAL(10,2), 0) AS LeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS DistributionCost,
	   CONVERT(DECIMAL(10,2), 0) AS Profit
INTO
	   #OperationsProfitOverview
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))  
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dp.ProductId,
	   dp.Name


/*
--------------- Update Sale Income Used to Calculate Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime) AS LeadDate,
	   dp.ProductId,
	   SUM(dld.SaleIncome) AS SaleIncome
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
GROUP BY
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime),
	   dp.ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET 
	  SaleIncome = CTE.SaleIncome
FROM
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId


--------------- Insert Sale Income Used to Calculate Earnings --------------------------
------ Insert Sale Income if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsProfitOverview
SELECT DISTINCT
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0.00 AS LeadIncome,
	   SUM(dld.SaleIncome) AS SaleIncome,
	   0.00 AS CancelledIncome,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   0.00 AS Profit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProfitOverview opo WITH (NOLOCK)
	   ON opo.LeadDate = DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime)
	   AND opo.ProductId = dp.ProductId
WHERE 
	   CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
	   AND opo.SaleIncome IS NULL
GROUP BY
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime),
	   dp.ProductId,
	   dp.Name


--------------- Update Cancelled Income Used to Calculate Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime) AS LeadDate,
	   dp.ProductId,
	   SUM(dld.CanceledIncome) AS CancelledIncome
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.CanceledDateTime) >= @StartDate AND CONVERT(Date,dld.CanceledDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.CanceledIncome = 1
GROUP BY
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime),
	   dp.ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET 
	  CancelledIncome = CTE.CancelledIncome
FROM
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId


--------------- Insert Cancelled Income Used to Calculate Earnings --------------------------
------ Insert Cancelled Income if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsProfitOverview
SELECT DISTINCT
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0.00 AS LeadIncome,
	   0.00 AS SaleIncome,
	   SUM(dld.CanceledIncome) AS CancelledIncome,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   0.00 AS Profit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProfitOverview opo WITH (NOLOCK)
	   ON opo.LeadDate = DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime)
	   AND opo.ProductId = dp.ProductId
WHERE 
	   CONVERT(Date,dld.CanceledDateTime) >= @StartDate AND CONVERT(Date,dld.CanceledDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.CanceledDateTime = 1
	   AND opo.CancelledIncome IS NULL
GROUP BY
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime),
	   dp.ProductId,
	   dp.Name


--------------- Update Earnings ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ProductId,
	   SUM(LeadIncome + SaleIncome + CancelledIncome) AS Earnings
 FROM
	   #OperationsProfitOverview WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET		
	  #OperationsProfitOverview.Earnings = CTE.Earnings
FROM 
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId
*/

--------------- Update Lead Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dld.ProductId,
	   SUM(ISNULL(dld.ProjectedAffiliateCost,0)) AS LeadCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET 
	  LeadCost = CTE.LeadCost
FROM
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId


--------------- Insert Lead Cost --------------------------
----- Insert Lead Cost where there's no lead date in the temp table ----------
INSERT INTO
	   #OperationsProfitOverview
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   --0.00 AS LeadIncome,
	   --0.00 AS SaleIncome,
	   --0.00 AS CancelledIncome,
	   0.00 AS Earnings,
	   SUM(ISNULL(dld.ProjectedAffiliateCost,0)) AS LeadCost,
	   NULL AS DistributionCost,
	   0.00 AS Profit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProfitOverview opo WITH (NOLOCK)
	   ON opo.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opo.ProductId = dp.ProductId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))   
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))  
	   AND dt.OldTenantId = @TenantID
	   AND opo.LeadCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dp.ProductId,
	   dp.Name


--------------- Update Distiribution Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dld.ProductId,
	   SUM(ISNULL(dld.ProjectedDistributionCost,0)) AS DistributionCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET 
	  DistributionCost = CTE.DistributionCost
FROM
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId


--------------- Insert Distribution Cost --------------------------
----- Insert Distribution Cost where there's no lead date in the temp table ----------
INSERT INTO
	   #OperationsProfitOverview
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   SUM(ISNULL(dld.ProjectedDistributionCost,0)) AS DistributionCost,
	   0.00 AS Profit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProfitOverview opo WITH (NOLOCK)
	   ON opo.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opo.ProductId = dp.ProductId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))   
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))  
	   AND dt.OldTenantId = @TenantID
	   AND opo.DistributionCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dp.ProductId,
	   dp.Name

--------------- Update Profit ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ProductId,
	   SUM(Earnings - LeadCost - DistributionCost) AS Profit
 FROM
	   #OperationsProfitOverview WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ProductId
)
UPDATE 
	  #OperationsProfitOverview
SET		
	  #OperationsProfitOverview.Profit = CTE.Profit
FROM 
	  #OperationsProfitOverview opo WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opo.LeadDate
	  AND CTE.ProductId = opo.ProductId


------------- Insert missing dates into Tep Tbl ---------------
DECLARE @MinProduct INT,
	    @MaxProduct INT

SET @MinProduct = (SELECT MIN(ProductId) FROM #OperationsProfitOverview)
SET @MaxProduct = (SELECT MAX(ProductId) FROM #OperationsProfitOverview)

WHILE @MinProduct <= @MaxProduct

BEGIN

;WITH CTE AS
(
SELECT  
	  Month_Year_Name, 
	  @MinProduct AS MinProd
FROM 
	  Calendar cal WITH (NOLOCK)
WHERE
	  cal.Month_Year_Name NOT IN (
								  SELECT LeadDate	
								  FROM #OperationsProfitOverview opo WITH (NOLOCK)
								  WHERE ProductId = @MinProduct
								 )
	  AND CONVERT(DATE,Calendar_Date) >= @StartDate AND CONVERT(DATE,Calendar_Date) <= @EndDate
)
INSERT INTO #OperationsProfitOverview
SELECT DISTINCT
	   cte.Month_Year_Name, 
	   opo.ProductId,
	   opo.Product,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   0.00 AS DistriutionCost,
	   0.00 AS Profit
FROM
	  CTE
INNER JOIN
	   #OperationsProfitOverview opo WITH (NOLOCK)
	   ON CTE.MinProd = opo.ProductId
	   
SET @MinProduct = (SELECT MIN(ProductId) FROM #OperationsProfitOverview WHERE @MinProduct < ProductId)

END


--------------- Select records for the report -------------------
SELECT  
	   LEFT(LeadDate, 3) + '-' + RIGHT(CAST(YEAR(LeadDate) as CHAR(4)), 2) AS LeadDate,
	   Product,
	   Profit
FROM 
	   #OperationsProfitOverview WITH (NOLOCK)
ORDER BY
	   YEAR(LeadDate),
	   MONTH(LeadDate)  ASC


RETURN

---------- Drop #OperationsProfitOverview Temp table -------------------
IF  OBJECT_ID (N'tempdb..#OperationsProfitOverview') IS NOT NULL
DROP TABLE #OperationsProfitOverview


END