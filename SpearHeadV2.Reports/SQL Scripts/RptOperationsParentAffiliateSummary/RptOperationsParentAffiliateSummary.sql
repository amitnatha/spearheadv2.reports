USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptOperationsParentAffiliateSummary]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptOperationsParentAffiliateSummary]
GO


CREATE PROCEDURE [Reports].[RptOperationsParentAffiliateSummary]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@ParentAffiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-3,@StartDate) -- Set the start date to 3 months prior to the month selected



--------------- Insert Leads Received -------------------
SELECT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dpa.ParentAffiliateId,
	   dpa.Title AS ParentAffiliate,
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived, 
	   0 AS LeadsAccepted,
	   CONVERT(DECIMAL(10,2), 0) AS LeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   CONVERT(DECIMAL(10,2), 0) AS DistributionCost,
	   CONVERT(DECIMAL(10,2), 0) AS Profit,
	   0 AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   0 AS IsActualDistCost
INTO   #OperationsParentAffiliate
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
 GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dpa.ParentAffiliateId,
	   dpa.Title,
	   dp.ProductId,
	   dp.Name


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dld.ParentAffiliateId,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
 GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ParentAffiliateId,
	   dld.ProductId
) 
UPDATE 
	  #OperationsParentAffiliate
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #OperationsParentAffiliate opa WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opa.LeadDate = CTE.LeadDate
	  AND opa.ParentAffiliateId = CTE.ParentAffiliateId
	  AND opa.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
---- Insert Leads Accepted if the date is not in the temp tbl -----
INSERT INTO 
	   #OperationsParentAffiliate
SELECT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dpa.ParentAffiliateId,
	   dpa.Title AS ParentAffiliate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted, 
	   0.00 AS LeadCost,
	   0.00 AS Earnings,
	   0.00 AS DistributionCost,
	   0.00 AS Profit,
	   0 AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   0 AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #OperationsParentAffiliate opa WITH (NOLOCK)
	   ON opa.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opa.ParentAffiliateId = dpa.ParentAffiliateId
	   AND opa.ProductId = dp.ProductId 
WHERE 
	   dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND opa.LeadsAccepted IS NULL
GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dpa.ParentAffiliateId,
	   dpa.Title,
	   dp.ProductId,
	   dp.Name


--------------- Update Lead Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
			ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
			END AS LeadCost,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualLeadCost,
	   dld.ParentAffiliateId,
	   dld.ProductId
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ParentAffiliateId,
	   dld.ProductId,
	   ActualAffiliateCost
)
UPDATE 
	  #OperationsParentAffiliate
SET 
	  LeadCost = CTE.LeadCost,
	  IsActualLeadCost = CTE.IsActualLeadCost
FROM
	  #OperationsParentAffiliate opa WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opa.LeadDate
	  AND CTE.ParentAffiliateId = opa.ParentAffiliateId
	  AND CTE.ProductId = opa.ProductId


--------------- Insert Lead Cost --------------------------
------ Insert Lead Cost if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsParentAffiliate
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dpa.ParentAffiliateId,
	   dpa.Title AS ParentAffiliate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
			ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
			END AS LeadCost,
	   0.00 AS Earnings,
	   0.00 AS DistributionCost,
	   0.00 AS Profit,
	   0 AS IsActualEarnings,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualLeadCost,
	   0 AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #OperationsParentAffiliate opa WITH (NOLOCK)
	   ON opa.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opa.ParentAffiliateId = dpa.ParentAffiliateId
	   AND opa.ProductId = dp.ProductId 
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND opa.LeadCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dpa.ParentAffiliateId,
	   dpa.Title,
	   dp.ProductId,
	   dp.Name,
	   ActualAffiliateCost


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dld.ParentAffiliateId,
	   dld.ProductId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ParentAffiliateId,
	   dld.ProductId,
	   ActualEarnings
)
UPDATE 
	  #OperationsParentAffiliate
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #OperationsParentAffiliate opa WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opa.LeadDate
	  AND CTE.ParentAffiliateId = opa.ParentAffiliateId
	  AND CTE.ProductId = opa.ProductId 

	  
--------------- Insert Earnings --------------------------
------ Insert Earnings if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsParentAffiliate
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dpa.ParentAffiliateId,
	   dpa.Title AS ParentAffiliate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0.00 AS LeadCost,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   0.00 AS DistributionCost,
	   0.00 AS Profit,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   0 AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #OperationsParentAffiliate opa WITH (NOLOCK)
	   ON opa.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opa.ParentAffiliateId = dpa.ParentAffiliateId
	   AND opa.ProductId = dp.ProductId 
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND opa.Earnings IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dpa.ParentAffiliateId,
	   dpa.Title,
	   dp.ProductId,
	   dp.Name,
	   ActualEarnings


--------------- Update Distribution Costs --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dld.ParentAffiliateId,
	   dld.ProductId,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
			ELSE SUM(ISNULL(ProjectedDistributionCost,0)) 
			END AS DistCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dld.ParentAffiliateId,
	   dld.ProductId,
	   ActualDistributionCost
)
UPDATE 
	  #OperationsParentAffiliate
SET 
	  DistributionCost = CTE.DistCost,
	  IsActualDistCost = CTE.IsActualDistCost
FROM
	  #OperationsParentAffiliate opa WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opa.LeadDate
	  AND CTE.ParentAffiliateId = opa.ParentAffiliateId
	  AND CTE.ProductId = opa.ProductId 

--------------- Insert Distribution Costs --------------------------
------ Insert Distribution Costs if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsParentAffiliate
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dpa.ParentAffiliateId,
	   dpa.Title AS ParentAffiliate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0.00 AS LeadCost,
	   0.00 AS Earnings,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
			ELSE SUM(ISNULL(ProjectedDistributionCost,0)) 
			END AS DistCost,
	   0.00 AS Profit,
	   0 AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #OperationsParentAffiliate opa WITH (NOLOCK)
	   ON opa.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
	   AND opa.ParentAffiliateId = dpa.ParentAffiliateId
	   AND opa.ProductId = dp.ProductId 
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND opa.DistributionCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dpa.ParentAffiliateId,
	   dpa.Title,
	   dp.ProductId,
	   dp.Name,
	   ActualDistributionCost


--------------- Update Profit ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ParentAffiliateId,
	   ProductId,
	   SUM(ISNULL(Earnings,0) - ISNULL(LeadCost,0) - ISNULL(DistributionCost,0)) AS Profit
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ParentAffiliateId,
	   ProductId
)
UPDATE 
	  #OperationsParentAffiliate
SET		
	  #OperationsParentAffiliate.Profit = CTE.Profit
FROM 
	  #OperationsParentAffiliate opa WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opa.LeadDate
	  AND CTE.ParentAffiliateId = opa.ParentAffiliateId
	  AND CTE.ProductId = opa.ProductId



----------------- Insert Records into Final Tbl ---------------------
-------- Insert month being reported on --------------
SELECT
	  LeadDate,
	  ParentAffiliate,
	  SUM(LeadsReceived) AS LeadsReceived,
	  SUM(LeadsAccepted) AS LeadsAccepted,
	  0 AS AvgLeadsReceived,
	  0 AS AvgLeadsAccepted,
	  SUM(Earnings) AS Earnings,
	  SUM(LeadCost) AS AffiliateCost,
	  SUM(DistributionCost) AS DistributionCost,
	  SUM(Profit) AS Profit,
	  CONVERT(DECIMAL(10,2), 0) AS PPL,
	  CONVERT(DECIMAL(10,2), 0) AS CPL,
	  CONVERT(DECIMAL(10,2), 0) AS CurrentEPL,
	  CONVERT(DECIMAL(10,2), 0) AS [3rdPreviousEPL],
	  CONVERT(DECIMAL(10,2), 0) AS [2ndPreviousEPL],
	  CONVERT(DECIMAL(10,2), 0) AS [PreviousEPL],
	  0 AS [3rdPreviousLeadsAccepted],
	  CONVERT(DECIMAL(10,2), 0) AS [3rdPreviousEarnings],
	  0 AS [2ndPreviousLeadsAccepted],
	  CONVERT(DECIMAL(10,2), 0) AS [2ndPreviousEarnings],
	  0 AS [PreviousLeadsAccepted],
	  CONVERT(DECIMAL(10,2), 0) AS [PreviousEarnings],
	  IsActualEarnings,
	  IsActualLeadCost,
	  IsActualDistCost,
	  0 AS IsActualEarnings3Rd,
	  0 AS IsActualEarnings2Nd,
	  0 AS IsActualEarningsPrev
INTO
	  #OperationsParentAffiliateRptSelection
FROM 
	  #OperationsParentAffiliate opa WITH (NOLOCK)
WHERE 
	  LeadDate = DATENAME(MONTH, @EndDate) + ' ' + DATENAME(YEAR,@EndDate)
GROUP BY
	  ParentAffiliate,
	  LeadDate,
	  IsActualEarnings,
	  IsActualLeadCost,
	  IsActualDistCost


------------- Update Avg Leads ------------------
;WITH CTE AS
(
 SELECT
	   ParentAffiliate, 
	   SUM(LeadsReceived) / 3 AS AvgLeadsReceived,
	   SUM(leadsAccepted) / 3 AS AvgLeadsAccepted
 FROM 
	  #OperationsParentAffiliate
 WHERE
	  LeadDate <> DATENAME(MONTH, @EndDate) + ' ' + DATENAME(YEAR,@EndDate) --- Exclude month being reported on
 GROUP BY
	  ParentAffiliate
)
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET
	  AvgLeadsReceived = CTE.AvgLeadsReceived,
	  AvgLeadsAccepted = CTE.AvgLeadsAccepted
FROM 
	  #OperationsParentAffiliateRptSelection opar
INNER JOIN	
	  CTE WITH (NOLOCK)
	  ON CTE.ParentAffiliate = opar.ParentAffiliate


--------------- Update Profit Per Lead ---------------------
------ Only inlude month being reported on --------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(Profit) / SUM(LeadsAccepted) AS PPL
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE 
	   LeadsAccepted <> 0
	   AND LeadDate = DATENAME(MONTH, @EndDate) + ' ' + DATENAME(YEAR,@EndDate)
 GROUP BY
	   ParentAffiliate
)
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET		
	  #OperationsParentAffiliateRptSelection.PPL = CTE.PPL
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.ParentAffiliate = opar.ParentAffiliate



--------------- Update Cost Per Lead -------------------
------ Only Update for month reporting on ----------
;WITH CTE AS
(
SELECT DISTINCT
	   ParentAffiliate,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0 ELSE SUM(LeadCost) / SUM(LeadsAccepted) END AS CostPerLead
FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
WHERE
	   LeadDate = DATENAME(MONTH, @EndDate) + ' ' + DATENAME(YEAR,@EndDate)
GROUP BY
	   ParentAffiliate
)
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET		
	  #OperationsParentAffiliateRptSelection.CPL = CTE.CostPerLead
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.ParentAffiliate = opar.ParentAffiliate



--------------- Update Earnings Per Lead -------------------
----- Only update for month being reported on ----------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsAccepted) END AS EPL -- Set EPL to 0 if there were no leads sent for a particular day
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH, @EndDate) + ' ' + DATENAME(YEAR,@EndDate)
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  CurrentEPL = CTE.EPL
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update Earnings Per Lead (3rd Previous Month) -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsAccepted) END AS EPL, -- Set EPL to 0 if there were no leads sent for a particular day
	   IsActualEarnings
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH, @StartDate) + ' ' + DATENAME(YEAR,@StartDate)
 GROUP BY
	   ParentAffiliate,
	   IsActualEarnings
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [3rdPreviousEPL] = CTE.EPL,
	  IsActualEarnings3Rd = CTE.IsActualEarnings
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update Earnings Per Lead (2nd Previous Month) -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsAccepted) END AS EPL, -- Set EPL to 0 if there were no leads sent for a particular day
	   IsActualEarnings
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-2,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-2,@EndDate))
 GROUP BY
	   ParentAffiliate,
	   IsActualEarnings
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [2ndPreviousEPL] = CTE.EPL,
	  IsActualEarnings2Nd = CTE.IsActualEarnings
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update Earnings Per Lead (Previous Month) -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   CASE WHEN SUM(LeadsAccepted) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsAccepted) END AS EPL, -- Set EPL to 0 if there were no leads sent for a particular day
	   IsActualEarnings
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-1,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-1,@EndDate))
 GROUP BY
	   ParentAffiliate,
	   IsActualEarnings
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  PreviousEPL = CTE.EPL,
	  IsActualEarningsPrev = CTE.IsActualEarnings
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update 3rd Previous Leads Accepted -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(LeadsAccepted) AS [3rdPreviousLeadsAccepted]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-3,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-3,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [3rdPreviousLeadsAccepted] = CTE.[3rdPreviousLeadsAccepted]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update 3rd Previous Earnings -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(Earnings) AS [3rdPreviousEarnings]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-3,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-3,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [3rdPreviousEarnings] = CTE.[3rdPreviousEarnings]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update 2nd Previous Leads Accepted -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(LeadsAccepted) AS [2ndPreviousLeadsAccepted]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-2,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-2,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [2ndPreviousLeadsAccepted] = CTE.[2ndPreviousLeadsAccepted]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update 2nd Previous Earnings -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(Earnings) AS [2ndPreviousEarnings]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-2,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-2,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [2ndPreviousEarnings] = CTE.[2ndPreviousEarnings]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update Previous Leads Received -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(LeadsAccepted) AS [PreviousLeadsAccepted]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-1,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-1,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [PreviousLeadsAccepted] = CTE.[PreviousLeadsAccepted]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Update Previous Earnings -------------------
;WITH CTE AS 
(
 SELECT 
	   ParentAffiliate,
	   SUM(Earnings) AS [PreviousEarnings]
 FROM
	   #OperationsParentAffiliate WITH (NOLOCK)
 WHERE
	   LeadDate = DATENAME(MONTH,DATEADD(m,-1,@EndDate))  + ' ' + DATENAME(YEAR,DATEADD(m,-1,@EndDate))
 GROUP BY
	   ParentAffiliate
) 
UPDATE 
	  #OperationsParentAffiliateRptSelection
SET 
	  [PreviousEarnings] = CTE.[PreviousEarnings]
FROM 
	  #OperationsParentAffiliateRptSelection opar WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opar.ParentAffiliate = CTE.ParentAffiliate


--------------- Select records for the report -------------------
SELECT  
	  ParentAffiliate,
	  NULL AS Product,
	  LeadsReceived,
	  LeadsAccepted,
	  AvgLeadsReceived,
	  AvgLeadsAccepted,
	  Earnings,
	  AffiliateCost,
	  DistributionCost,
	  Profit,
	  PPL,
	  CPL,
	  CurrentEPL,
	  [3rdPreviousEPL] AS ThirdPreviousEPL,
	  [2ndPreviousEPL] AS SecondPreviousEPL,
	  PreviousEPL,
	  [3rdPreviousLeadsAccepted] AS ThirdPrevLeadsAccepted,
	  [3rdPreviousEarnings] AS ThirdPreviousEarnings,
	  [2ndPreviousLeadsAccepted] AS SecondPrevLeadsAccepted,
	  [2ndPreviousEarnings] AS SecondPreviousEarnings,
	  [PreviousLeadsAccepted] AS PrevLeadsAccepted,
	  [PreviousEarnings] AS PreviousEarnings,
	  IsActualEarnings,
	  IsActualLeadCost,
	  IsActualDistCost,
	  IsActualEarnings3Rd,
	  IsActualEarnings2Nd,
	  IsActualEarningsPrev
FROM 
	  #OperationsParentAffiliateRptSelection WITH (NOLOCK)
UNION ALL
------ Only return earnings per prod for month being reported on
SELECT
	  ParentAffiliate,
	  Product,
	  NULL AS LeadsReceived,
	  NULL AS LeadsAccepted,
	  0 AS AvgLeadsReceived,
	  0 AS AvgLeadsAccepted,
	  SUM(Earnings),
	  0.00 AS AffiliateCost,
	  NULL AS DistributionCost,
	  NULL AS Profit,
	  0.00 AS PPL,
	  0.00 AS CPL,
	  0.00 AS CurrentEPL,
	  0.00 AS ThirdPreviousEPL,
	  0.00 AS SecondPreviousEPL,
	  0.00 AS PreviousEPL,
	  NULL AS ThirdPrevLeadsAccepted,
	  NULL AS ThirdPreviousEarnings,
	  NULL AS SecondPrevLeadsAccepted,
	  NULL AS SecondPreviousEarnings,
	  NULL AS PrevLeadsAccepted,
	  NULL AS PreviousEarnings,
	  IsActualEarnings,
	  IsActualLeadCost,
	  IsActualDistCost,
	  0 AS IsActualEarnings3Rd,
	  0 AS IsActualEarnings2Nd,
	  0 AS IsActualEarningsPrev
FROM
	  #OperationsParentAffiliate WITH (NOLOCK)
WHERE
	  LeadDate = DATENAME(MONTH,@EndDate)  + ' ' + DATENAME(YEAR,@EndDate)
GROUP BY
	  ParentAffiliate, 
	  Product,
	  IsActualEarnings,
	  IsActualLeadCost,
	  IsActualDistCost


RETURN

---------- Drop #OperationsParentAffiliate Temp table -------------------
IF  OBJECT_ID (N'tempdb..#OperationsParentAffiliate') IS NOT NULL
DROP TABLE #OperationsParentAffiliate

---------- Drop #OperationsParentAffiliateRptSelection Temp table -------------------
IF  OBJECT_ID (N'tempdb..#OperationsParentAffiliateRptSelection') IS NOT NULL
DROP TABLE #OperationsParentAffiliateRptSelection

END