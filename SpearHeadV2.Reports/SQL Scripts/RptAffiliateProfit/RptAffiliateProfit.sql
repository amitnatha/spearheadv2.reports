USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateProfit]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateProfit]
GO


CREATE PROCEDURE [Reports].[RptAffiliateProfit]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@ParentAffiliate NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER,
@Affiliate NVARCHAR(MAX)

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE,
		@ReportName NVARCHAR (50)


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-5,@StartDate) -- Set the start date to 6 months prior to the month selected


BEGIN TRY

--------------- Insert Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   da.AffiliateId,
	   da.Name AS Affiliate,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))   
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   da.AffiliateId,
	   da.Name,
	   ActualEarnings
)
SELECT DISTINCT
	   LeadDate,
	   AffiliateId,
	   Affiliate,
	   SUM(Earnings) AS Earnings,
	   CONVERT(DECIMAL(10,2), 0) AS LeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS DistCost,
	   CONVERT(DECIMAL(10,2), 0) AS Profit,
	   CONVERT(DECIMAL(10,3), 0) AS Margin,
	   MAX(IsActualEarnings) AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   0 AS IsActualDistCost	   
INTO #AffiliateProfit
FROM	
	 CTE WITH (NOLOCK)
GROUP BY
	 LeadDate,
	 AffiliateId,
	 Affiliate

--------------- Update Lead Cost --------------------------
;WITH CTE AS
(
SELECT 
	  LeadDate,
	  SUM(LeadCost) AS LeadCost,
	  MAX(IsActualLeadCost) AS IsActualLeadCost,
	  AffiliateId
FROM (
		SELECT DISTINCT
			   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS LeadCost,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualLeadCost,
			   dld.AffiliateId
		FROM 
			   LeadDetail dld WITH (NOLOCK)
		INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
			   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
			   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
			   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))  
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
		GROUP BY
			   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
			   dld.AffiliateId,
			   ActualAffiliateCost
	) DervTbl
GROUP BY
	LeadDate,
	AffiliateId
)

UPDATE 
	  #AffiliateProfit
SET 
	  LeadCost = CTE.LeadCost,
	  IsActualLeadCost = CTE.IsActualLeadCost
FROM
	  #AffiliateProfit ap WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = ap.LeadDate
	  AND CTE.AffiliateId = ap.AffiliateId


----------------------- Insert Lead Cost ----------------------------
------ Insert Lead Cost if the date is not in the Temp Tbl ----------
INSERT INTO 
	   #AffiliateProfit 
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   da.AffiliateId,
	   da.Name AS Affiliate,
	   0.00 AS Earnings,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
			ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
			END AS LeadCost,
	   0.00 AS DistCost,
	   0.00 AS Profit,
	   0.00 AS Margin,
	   0 AS IsActualEarnigs,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualLeadCost,
	   0 AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN	 
	   #AffiliateProfit ap WITH (NOLOCK)
	   ON ap.AffiliateId = da.AffiliateId
	   AND DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) = ap.LeadDate
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))  
	   AND dt.OldTenantId = @TenantID
	   AND ap.LeadCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   da.AffiliateId,
	   da.Name,
	   ActualAffiliateCost

--------------- Update Dist Cost --------------------------
;WITH CTE AS
(
SELECT 
	  LeadDate,
	  SUM(DistCost) AS DistCost,
	  MAX(IsActualDistCost) AS IsActualDistCost,
	  AffiliateId
FROM (
		SELECT DISTINCT
			   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
			   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS DistCost,
			   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualDistCost,
			   dld.AffiliateId
		FROM 
			   LeadDetail dld WITH (NOLOCK)
		INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
			   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
			   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
			   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))  
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
		GROUP BY
			   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
			   dld.AffiliateId,
			   ActualDistributionCost
	) DervTbl
GROUP BY
	LeadDate,
	AffiliateId
)

UPDATE 
	  #AffiliateProfit
SET 
	  DistCost = CTE.DistCost,
	  IsActualLeadCost = CTE.IsActualDistCost
FROM
	  #AffiliateProfit ap WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = ap.LeadDate
	  AND CTE.AffiliateId = ap.AffiliateId


----------------------- Insert Dist Cost ----------------------------
------ Insert Dist Cost if the date is not in the Temp Tbl ----------
INSERT INTO 
	   #AffiliateProfit 
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   da.AffiliateId,
	   da.Name AS Affiliate,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
			ELSE SUM(ISNULL(ProjectedDistributionCost,0)) 
			END AS DistCost,
	   0.00 AS Profit,
	   0.00 AS Margin,
	   0 AS IsActualEarnigs,
	   0 AS IsActualLeadCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN	 
	   #AffiliateProfit ap WITH (NOLOCK)
	   ON ap.AffiliateId = da.AffiliateId
	   AND DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) = ap.LeadDate
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))    
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))  
	   AND dt.OldTenantId = @TenantID
	   AND ap.LeadCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   da.AffiliateId,
	   da.Name,
	   ActualDistributionCost


----------- Update Profit ----------------
;WITH CTE AS
(
SELECT 
	  LeadDate,
	  AffiliateId,
	  SUM(ISNULL(Earnings,0) - ISNULL(LeadCost,0) - ISNULL(DistCost,0)) AS Profit
FROM 
	  #AffiliateProfit WITH (NOLOCK)
GROUP BY
	  LeadDate,
	  AffiliateId
)
UPDATE 
	  #AffiliateProfit
SET 
	  Profit = CTE.Profit
FROM	  
	  #AffiliateProfit ap WITH (NOLOCK)
INNER JOIN 
	  CTE
	  ON CTE.LeadDate = ap.LeadDate
	  AND CTE.AffiliateId = ap.AffiliateId


----------- Update Margin ----------------
;WITH CTE AS
(
SELECT 
	  LeadDate,
	  AffiliateId,
	  CASE WHEN SUM(Earnings) = 0 THEN 0 ELSE SUM(Profit) / SUM(Earnings) END AS Margin
FROM 
	  #AffiliateProfit WITH (NOLOCK)
GROUP BY
	  LeadDate,
	  AffiliateId,
	  Profit
)
UPDATE 
	  #AffiliateProfit
SET 
	  Margin = CTE.Margin
FROM	  
	  #AffiliateProfit ap WITH (NOLOCK)
INNER JOIN 
	  CTE
	  ON CTE.LeadDate = ap.LeadDate
	  AND CTE.AffiliateId = ap.AffiliateId


------------- Insert missing dates into Temp Tbl ---------------
DECLARE @MinAffiliateId INT,
	    @MaxAffiliateId INT

SET @MinAffiliateId = (SELECT MIN(AffiliateId) FROM #AffiliateProfit)
SET @MaxAffiliateId = (SELECT MAX(AffiliateId) FROM #AffiliateProfit)

WHILE @MinAffiliateId <= @MaxAffiliateId

BEGIN

;WITH CTE AS
(
SELECT  
	  Month_Year_Name, 
	  @MinAffiliateId AS AffiliateId
FROM 
	  Calendar cal WITH (NOLOCK)
WHERE
	  Month_Year_Name NOT IN (
											  SELECT LeadDate	
											  FROM #AffiliateProfit WITH (NOLOCK)
											  WHERE AffiliateId = @MinAffiliateId
											 )
	  AND CONVERT(DATE,Calendar_Date) >= @StartDate AND CONVERT(DATE,Calendar_Date) <= @EndDate
	  AND cal.Day_Number = 1
)
INSERT INTO #AffiliateProfit
SELECT DISTINCT
	   CTE.Month_Year_Name AS LeadDate,		
	   ap.AffiliateId,
	   ap.Affiliate,
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   0.00 AS DistCost,
	   0.00 AS Profit,
	   0.00 AS Margin,
	   0 AS IsActualEarnings,
	   0 AS IsActualLeadCost,
	   0 AS IsActualDistCost
FROM
	  CTE
INNER JOIN
	   #AffiliateProfit ap WITH (NOLOCK)
	   ON CTE.AffiliateId = ap.AffiliateId
	   
SET @MinAffiliateId = (SELECT MIN(AffiliateId) FROM #AffiliateProfit WHERE @MinAffiliateId < AffiliateId)

END

--------------- Select records for the report -------------------
SELECT  
	   LeadDate,
	   Affiliate,
	   Earnings,
	   LeadCost AS Costs,
	   DistCost,
	   Profit,
	   Margin,
	   IsActualEarnings,
	   IsActualLeadCost,
	   IsActualDistCost
FROM 
	   #AffiliateProfit WITH (NOLOCK)
ORDER BY
	   YEAR(LeadDate),
	   MONTH(LeadDate)


RETURN


---------- Drop #AffiliateProfit Temp table -------------------
IF  OBJECT_ID (N'tempdb..#AffiliateProfit') IS NOT NULL
DROP TABLE #AffiliateProfit

END TRY 

----------------- Insert Into Exception Table if theres an error --------------------
BEGIN CATCH

INSERT INTO
	   DataMartStaging.dbo.SsrsErrorException
( 
 SpName,
 ReportName,
 TenantParameter,
 Parameter1,
 Parameter2,
 Parameter3,
 Parameter4,
 Parameter5,
 Parameter6,
 Parameter7,
 Parameter8,
 ErrorDescription,
 LineNumber,
 InsertDateTime
)
SELECT 
	  ERROR_PROCEDURE(),
	  @ReportName,
	  @TenantID,
	  'Year: ' + CONVERT(NVARCHAR(MAX), @Year, 110),
	  'Month: ' + CONVERT(NVARCHAR(MAX), @Month, 110),
	  'ParentAffiliate: ' + CONVERT(NVARCHAR(MAX), @ParentAffiliate, 110),
	  'AffiliateManager: ' + CONVERT(NVARCHAR(MAX), @AffiliateManager, 110),
	  'Product: ' + CONVERT(NVARCHAR(MAX), @Product, 110),
	  'Affiliate: ' + CONVERT(NVARCHAR(MAX), @Affiliate, 110),
	  NULL AS Parameter7,
	  NULL AS Parameter8,
	  ERROR_MESSAGE(),
	  ERROR_LINE(),
	  GETDATE()

------ Force the SQL Job to fail. The job would be shown as success if it had failed after catching the error	    
RAISERROR('Please refer to the SsrsErrorException table for the full error message',16,1)
		   
END CATCH

END