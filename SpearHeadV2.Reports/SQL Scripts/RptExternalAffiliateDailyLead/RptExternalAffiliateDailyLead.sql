USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptExternalAffiliateDailyLead]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptExternalAffiliateDailyLead]
GO


CREATE PROCEDURE [Reports].[RptExternalAffiliateDailyLead]
@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER,
@AffiliateGUID NVARCHAR(MAX)

AS BEGIN


----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS LeadsAccepted
INTO 
	   #ExternalAffiliateDailyLead
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dp.ProductId



--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateDailyLead
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ExternalAffiliateDailyLead eaml WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eaml.LeadDate = CTE.LeadDate
	  AND eaml.AffiliateShortCode = CTE.ShortCode
	  AND eaml.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #ExternalAffiliateDailyLead
 SELECT DISTINCT
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateDailyLead eaml WITH (NOLOCK)
	   ON eaml.LeadDate = CONVERT(DATE, dld.[Date])
	   AND eaml.AffiliateShortCode = dasc.Code
	   AND eaml.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND eaml.LeadsAccepted IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadDate,
	   AffiliateShortCode,
	   [Description],
	   Product,
	   LeadsReceived,
	   LeadsAccepted
FROM 
	  #ExternalAffiliateDailyLead WITH (NOLOCK)

RETURN

---------- Drop #ExternalAffiliateDailyLead Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ExternalAffiliateDailyLead') IS NOT NULL
DROP TABLE #ExternalAffiliateDailyLead


END