USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptMonthlyLeadsToClient]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptMonthlyLeadsToClient]
GO


CREATE PROCEDURE [Reports].[RptMonthlyLeadsToClient]
@StartDate DATE,
@EndDate DATE,
@TenantID UNIQUEIDENTIFIER

AS BEGIN

-------------- Insert Leads Sent --------------------
SELECT DISTINCT 
	   ddis.DistributionPointId,
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSent,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName,	
	   ddis.DistributionPointName AS DistributionPoint,
	   0 AS Sales,
	   0 AS LeadsAccepted
INTO 
	   #MonthlyLeadsToClient
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE  
 	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate 
	   AND dc.Name NOT LIKE '%*** test%'
	   AND dt.OldTenantId = @TenantID
	   AND IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY	
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dp.Name,
	   dc.Name,
	   ddis.DistributionPointName,
	   ddis.DistributionPointId


----------------- Update Leads Accepted ----------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   ddp.DistributionPointId,
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS AcceptedDateTime,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
 INNER JOIN	 
	   Product dp WITH (NOLOCK)
	   ON dp.ProductId = dld.ProductId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE  
	   IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) BETWEEN @StartDate AND @EndDate
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dld.ProductId,
	   ddp.DistributionPointId,
	   dp.Name,
	   dc.Name
) 
UPDATE 
	  #MonthlyLeadsToClient
SET 
	  LeadsAccepted = ISNULL(CTE.Cnt, 0)
FROM 
	  #MonthlyLeadsToClient dmltc
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dmltc.DistributionPointId = CTE.DistributionPointId
	  AND CTE.AcceptedDateTime = dmltc.LeadDate
	  AND CTE.ProductName = dmltc.ProductName
	  AND CTE.ClientName = dmltc.ClientName


----------------- Update Sales ----------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   ddp.DistributionPointId,
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SoldDateTime,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
 INNER JOIN	 
	   Product dp WITH (NOLOCK)
	   ON dp.ProductId = dld.ProductId
 INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   IsSold = 1 
	   --AND IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) BETWEEN @StartDate AND @EndDate
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dld.ProductId,
	   ddp.DistributionPointId,
	   dp.Name,
	   dc.Name
) 
UPDATE 
	  #MonthlyLeadsToClient
SET 
	  Sales = ISNULL(CTE.Cnt, 0)
FROM 
	  #MonthlyLeadsToClient dmltc
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dmltc.DistributionPointId = CTE.DistributionPointId
	  AND CTE.SoldDateTime = dmltc.LeadDate
	  AND CTE.ProductName = dmltc.ProductName
	  AND CTE.ClientName = dmltc.ClientName
	  

-------------------- Select Records for Report -----------------------
SELECT 
	  *
FROM 
	  #MonthlyLeadsToClient WITH (NOLOCK)
ORDER BY 
	  CONVERT(DATETIME, LeadDate, 103) ASC

RETURN

--------------------- Drop Monthly Leads To Client Temp Table ------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#MonthlyLeadsToClient') AND type in (N'U'))
DROP TABLE #MonthlyLeadsToClient

END
