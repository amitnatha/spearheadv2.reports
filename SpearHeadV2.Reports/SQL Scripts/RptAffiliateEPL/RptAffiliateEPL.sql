USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateEPL]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateEPL]
GO


CREATE PROCEDURE [Reports].[RptAffiliateEPL]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @StartDate = DATEADD(MONTH,-11,@StartDate)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+12,0)))


--------------- Insert Number of Leads Received --------------------------
SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived, 
	   da.Name AS AffiliateName
INTO 
	   #AffiliateEPLCnt
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   da.Name


------------- Insert Pivot records into temp table -------------------------
SELECT 
	  *
INTO  
	  #AffiliateEPLPivot
FROM (
      SELECT 
			LeadYear,
			DATENAME(MONTH,CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)) AS LeadMonth, 
			LeadsReceived,
			AffiliateName
	  FROM
		    #AffiliateEPLCnt WITH (NOLOCK)
) AS dervtbl
PIVOT
(
 SUM(LeadsReceived)
 FOR LeadMonth IN (january, february, march, april, may, june, july, august, september, october, november, december)
)AS piv;


--------------- Insert NULL records in Pivot Table that does not have a record for a particular year -----------------
DECLARE @MinYear NVARCHAR (10),
@MaxYear NVARCHAR (10)

SELECT @MinYear = (Select MIN(LeadYear) FROM #AffiliateEPLPivot WITH (NOLOCK))
SELECT @MaxYear = (Select MAX(LeadYear) FROM #AffiliateEPLPivot WITH (NOLOCK))

SELECT @MinYear = CAST(@MinYear + '01' + '01' AS DATE)
SELECT @MaxYear = CAST(@MaxYear + '01' + '01' AS DATE)

;WITH CTE AS
(
SELECT
	  COUNT(*) OVER (PARTITION BY AffiliateName) AS TotalRecords,
	  AffiliateName,
	  LeadYear
FROM
	  #AffiliateEPLPivot WITH (NOLOCK) 
GROUP BY
	  AffiliateName, 
	  leadYear
)
INSERT INTO	#AffiliateEPLPivot
(
 LeadYear,
 AffiliateName
)
SELECT 
	  NewYear = CASE 
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MaxYear) >=1 
				THEN DATEPART(YEAR,@MaxYear)
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MinYear) <1 
				THEN DATEPART(YEAR,@MinYear)  
				END,
	  CTE.AffiliateName
FROM
	  #AffiliateEPLPivot piv WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON CTE.AffiliateName = piv.AffiliateName
WHERE
	  CTE.TotalRecords < 2

	  
------------------------ Insert data into Final Table using Unpivot ------------------------
SELECT
	  aep.LeadYear, 
	  aep.AffiliateName, 
	  MONTH(dervtbl.LeadMonth + ' 1 2010') AS LeadMonth,
	  LeadsReceived = 
					 CASE dervtbl.LeadMonth
						  when 'January' then aep.January
						  when 'February' then aep.February
						  when 'March' then aep.March
						  when 'April' then aep.April
						  when 'May' then aep.May
						  when 'June' then aep.June
						  when 'July' then aep.July
						  when 'August' then aep.August
						  when 'September' then aep.September
						  when 'October' then aep.October
						  when 'November' then aep.November
						  when 'December' then aep.December
					END,
	  CONVERT(DECIMAL(10,2), 0) AS Earnings,
	  CONVERT(DECIMAL(10,2), 0) AS EPL,
	  0 AS IsActualEarning
INTO #RptAffiliateEPLTbl
FROM (
	  SELECT LeadYear, AffiliateName, january, february, march, april, may, june, july, august, september, october, november, december
	  FROM #AffiliateEPLPivot aep WITH (NOLOCK)
  ) aep
CROSS JOIN (
			  select 'January' union all
			  select 'February' union all
			  select 'March' union all
			  select 'April' union all
			  select 'May' union all
			  select 'June' union all
			  select 'July' union all
			  select 'August' union all
			  select 'September' union all
			  select 'October' union all
			  select 'November' union all
			  select 'December'
			) dervtbl (LeadMonth)


--------------- Set Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   da.Name AS AffiliateName,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsDuplicate = 0
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   da.Name,
	   ActualEarnings
)
UPDATE 
	  #RptAffiliateEPLTbl
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarning = CTE.IsActualEarnings
FROM
	  #RptAffiliateEPLTbl aet WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.AffiliateName = aet.AffiliateName
	  AND CTE.LeadMonth = aet.LeadMonth
	  AND CTE.LeadYear = aet.LeadYear


--------------- Update EPL ---------------------
;WITH CteEPL AS 
(
 SELECT 
       LeadYear,
	   LeadMonth,
	   AffiliateName,
	   SUM(Earnings / LeadsReceived) AS EPL
 FROM
	   #RptAffiliateEPLTbl WITH (NOLOCK)
 GROUP BY
	   LeadYear,
	   LeadMonth,
	   AffiliateName
)
UPDATE 
	  #RptAffiliateEPLTbl
SET		
	  #RptAffiliateEPLTbl.EPL = cepl.EPL
FROM 
	  #RptAffiliateEPLTbl aet WITH (NOLOCK)
INNER JOIN 
	  CteEPL cepl WITH (NOLOCK)
	  ON cepl.LeadYear = aet.LeadYear
	  AND cepl.LeadMonth = aet.LeadMonth
	  AND cepl.AffiliateName = aet.AffiliateName




--------- Select Final Records for the report ------------------
SELECT
	  LeadYear,
	  LeadMonth,
	  LeadsReceived,
	  AffiliateName,
	  Earnings,
	  IsActualEarning,
	  ISNULL(EPL, 0) AS EPL
FROM
	  #RptAffiliateEPLTbl WITH (NOLOCK)
WHERE 
	  CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) >= @StartDate
	  AND CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) <= @EndDate
ORDER BY 
	  AffiliateName ASC

RETURN

---------- Drop Affiliate Earnings Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateEPLCnt') AND type in (N'U'))
DROP TABLE #AffiliateEPLCnt

---------- Drop Affiliate Earnings Pivot Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateEPLPivot') AND type in (N'U'))
DROP TABLE #AffiliateEPLPivot

---------- Drop RptAffiliateEarnings Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#RptAffiliateEPLTbl') AND type in (N'U'))
DROP TABLE #RptAffiliateEPLTbl

END
