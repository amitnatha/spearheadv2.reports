USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateLeads]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateLeads]
GO


CREATE PROCEDURE [Reports].[RptAffiliateLeads]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
set @StartDate = DATEADD(MONTH,-11,@StartDate)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+12,0)))


---------------- Insert Leads Received -----------------
SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   da.Name AS Affiliate,
	   dpa.Title AS ParentAffiliate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   dp.Name AS ProductName,
	   dau.Name AS AffiliateManager 
INTO 
	   #AffiliateLeadsCnt
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
	   AND dld.IsDuplicate = 0
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   dp.Name,
	   dau.Name,
	   da.Name,
	   dpa.Title



------------- Insert Pivot records into temp table -------------------------
SELECT 
	  *
INTO  
	  #AffiliateLeadsPivot
FROM (
      SELECT 
			LeadYear,
			DATENAME(MONTH,CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)) AS LeadMonth, 
			LeadsReceived,
			Affiliate
	  FROM
		    #AffiliateLeadsCnt WITH (NOLOCK)
) AS dervtbl
PIVOT
(
 SUM(LeadsReceived)
 FOR LeadMonth IN (january, february, march, april, may, june, july, august, september, october, november, december)
)AS piv;



--------------- Insert NULL records in Pivot Table that does not have a record for a particular year -----------------
DECLARE @MinYear NVARCHAR (10),
@MaxYear NVARCHAR (10)

SELECT @MinYear = (Select MIN(LeadYear) FROM #AffiliateLeadsPivot WITH (NOLOCK))
SELECT @MaxYear = (Select MAX(LeadYear) FROM #AffiliateLeadsPivot WITH (NOLOCK))

SELECT @MinYear = CAST(@MinYear + '01' + '01' AS DATE)
SELECT @MaxYear = CAST(@MaxYear + '01' + '01' AS DATE)

;WITH CTE AS
(
SELECT
	  COUNT(*) OVER (partition by Affiliate) AS TotalRecords,
	  Affiliate,
	  LeadYear
FROM
	  #AffiliateLeadsPivot WITH (NOLOCK) 
GROUP BY
	  Affiliate, 
	  leadyear
)
INSERT INTO	#AffiliateLeadsPivot
(
 LeadYear,
 Affiliate
)
SELECT 
	  NewYear = CASE 
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MaxYear) >=1 
				THEN DATEPART(YEAR,@MaxYear)
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MinYear) <1 
				THEN DATEPART(YEAR,@MinYear)  
				END,
	  cte.Affiliate
FROM
	  #AffiliateLeadsPivot piv WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON cte.Affiliate  = piv.Affiliate
WHERE
	  CTE.TotalRecords < 2



------------------------ Insert data into Final Table using Unpivot ------------------------
SELECT
	  alp.LeadYear, 
	  alp.Affiliate, 
	  MONTH(dervtbl.LeadMonth + ' 1 2010') AS LeadMonth,
	  Leads = 
			 CASE dervtbl.LeadMonth
				  when 'January' then alp.January
				  when 'February' then alp.February
				  when 'March' then alp.March
				  when 'April' then alp.April
				  when 'May' then alp.May
				  when 'June' then alp.June
				  when 'July' then alp.July
				  when 'August' then alp.August
				  when 'September' then alp.September
				  when 'October' then alp.October
				  when 'November' then alp.November
				  when 'December' then alp.December
	        END
INTO #RptAffiliateLeadsTbl
FROM (
	  SELECT LeadYear, Affiliate, january, february, march, april, may, june, july, august, september, october, november, december
	  FROM #AffiliateLeadsPivot alp WITH (NOLOCK)
  ) alp
CROSS JOIN (
			  select 'January' union all
			  select 'February' union all
			  select 'March' union all
			  select 'April' union all
			  select 'May' union all
			  select 'June' union all
			  select 'July' union all
			  select 'August' union all
			  select 'September' union all
			  select 'October' union all
			  select 'November' union all
			  select 'December'
			) dervtbl (LeadMonth)


--------- Select Final Records for the report ------------------
SELECT
	  LeadYear,
	  LeadMonth,
	  Affiliate,
	  ISNULL(Leads, 0) AS LeadsReceived
FROM
	  #RptAffiliateLeadsTbl WITH (NOLOCK)
WHERE 
	  CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) >= @StartDate
	  AND CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) <= @EndDate
ORDER BY 
	  Affiliate ASC

RETURN


---------- Drop Affiliate Leads Cnt Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateLeadsCnt') AND type in (N'U'))
DROP TABLE #AffiliateLeadsCnt

---------- Drop Affiliate Leads Pivot Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateLeadsPivot') AND type in (N'U'))
DROP TABLE #AffiliateLeadsPivot


---------- Drop RptAffiliateLeads Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#RptAffiliateLeadsTbl') AND type in (N'U'))
DROP TABLE #RptAffiliateLeadsTbl


END