USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptClientEarnings]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptClientEarnings]
GO


CREATE PROCEDURE [Reports].[RptClientEarnings]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@Client NVARCHAR(MAX),
@AccountManager NVARCHAR(MAX),
@DistributionPoint NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE,
	    @SpName NVARCHAR (50),
		@ReportName NVARCHAR (50)

SET @ReportName = 'RptClientEarnings'

SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0)))


BEGIN TRY

------------- Insert Leads Sent ------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointName,
	   dasc.Code,
	   dasc.[Description],
	   COUNT(DISTINCT dld.LeadId) AS MtdLeadsSent,
	   NULL AS ExclMTDLeadsAccepted,
	   NULL AS NonExclMTDLeadsAccepted,
	   NULL AS MTDSales,
	   NULL AS MTDConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS MtdEarnings,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDEarningsAct,
	   CONVERT(DECIMAL(10,2), 0) AS MtdLeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS MtdDistributionCost,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDCostsAct,
	   CONVERT(DECIMAL(10,2), 0) AS MtdProfit,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDProfitAct,
	   CONVERT(DECIMAL(10,2), 0) AS MtdEpl,
	   CONVERT(DECIMAL(10,2), 0) AS MtdCpl,
	   ddis.DistributionPointId,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName,
	   0 AS IsActualAffCost,
	   0 AS IsActualDistCost,
	   0 AS IsActualEarning
INTO 
	   #ClientEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.Name,
	   ddis.DistributionPointId,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointName

------------- Insert leads used for Exclusive (Lead accepted by 1 client) -------------
SELECT DISTINCT
	   OldLeadId
INTO #ExclusiveLeads
FROM 
	   LeadDetail AS DLD WITH (NOLOCK)
INNER JOIN 
	   Lead AS DL WITH (NOLOCK)
	   ON DLD.LeadId = DL.LeadId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND DLD.ProductId = @Product
	   AND DLD.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   --AND DLD.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,',')) -- Not needed. If only 1 client is selected in the filter, this condition will not bring back leads accepted only by 1 clients
	   --AND DLD.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,',')) -- Not needed. If only 1 client is selected in the filter, this condition will not bring back leads accepted only by 1 clients
	   AND DLD.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND IsAccepted = 1
	   AND IsValid = 1
GROUP BY
	   OldLeadId
HAVING 
	   COUNT(DLD.ClientId) = 1

------------- Insert leads used for Non-Exclusive (Lead accepted by more than 1 client) -------------
SELECT DISTINCT
	   OldLeadId
INTO #NonExclusiveLeads
FROM 
	   LeadDetail AS DLD WITH (NOLOCK)
INNER JOIN 
	   Lead AS DL WITH (NOLOCK)
	   ON DLD.LeadId = DL.LeadId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND DLD.ProductId = @Product
	   AND DLD.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   --AND DLD.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,',')) -- Not needed. If only 1 client is selected in the filter, this condition will not bring back leads accepted by other clients
	   --AND DLD.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,',')) -- Not needed. If only 1 client is selected in the filter, this condition will not bring back leads accepted by other clients
	   AND DLD.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND IsAccepted = 1
	   AND IsValid = 1
GROUP BY
	   OldLeadId
HAVING 
	   COUNT(DLD.ClientId) > 1


--------------- Set MTD Leads Accepted Exclusive -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN
	   #ExclusiveLeads AS DEL WITH (NOLOCK)
	   ON dl.OldLeadId = del.OldLeadId
 WHERE 
	   dld.IsAccepted = 1
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
)
UPDATE 
	  #ClientEarnings
SET 
	  ExclMTDLeadsAccepted = ISNULL(CTE.Cnt, 0)
FROM 
	  #ClientEarnings dce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dce.DistributionPointId = CTE.DistributionPointId
	  AND dce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND dce.LeadDate = CTE.LeadDate


--------------- Set MTD Leads Accepted Non-Exclusive -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN
	   #NonExclusiveLeads AS DNEL WITH (NOLOCK)
	   ON dl.OldLeadId = DNEL.OldLeadId
 WHERE 
	   dld.IsAccepted = 1
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
)
UPDATE 
	  #ClientEarnings
SET 
	  NonExclMTDLeadsAccepted = ISNULL(CTE.Cnt, 0)
FROM 
	  #ClientEarnings dce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dce.DistributionPointId = CTE.DistributionPointId
	  AND dce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND dce.LeadDate = CTE.LeadDate


--------------- Set MTD Sales -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 WHERE 
	   dld.IsSold = 1
	   AND dld.IsValid = 1
	   --AND dld.IsAccepted = 1 Lead might be sold & accepted on a different date
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
) 
UPDATE 
	  #ClientEarnings
SET 
	  MTDSales = ISNULL(CTE.Cnt, 0)
FROM 
	  #ClientEarnings dce
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dce.DistributionPointId = CTE.DistributionPointId
	  AND dce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND dce.LeadDate = CTE.LeadDate


---------- Insert Leads Sold 2015-10-06 -----------------
--- Insert Leads that were Sold for the month being reporting on but has not been sent in the same month -----
INSERT INTO 
	   #ClientEarnings
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointName,
	   dasc.Code,
	   dasc.[Description],
	   NULL AS MtdLeadsSent,
	   NULL AS ExclMTDLeadsAccepted,
	   NULL AS NonExclMTDLeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS MTDSales,
	   NULL AS MTDConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS MTDEarning,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDEarningsAct,
	   CONVERT(DECIMAL(10,2), 0) AS MTDLeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS MTDDistributionCost,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDCostsAct,
	   CONVERT(DECIMAL(10,2), 0) AS MTDProfit,
	   --CONVERT(DECIMAL(10,2), 0) AS MTDProfitAct,
	   CONVERT(DECIMAL(10,2), 0) AS MTDEpl,
	   CONVERT(DECIMAL(10,2), 0) AS MTDCpl,
	   ddis.DistributionPointId,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName,
	   0 AS IsActualAffCost,
	   0 AS IsActualDistCost,
	   0 AS IsActualEarning
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
LEFT OUTER JOIN
	   #ClientEarnings AS DCL WITH (NOLOCK)
	   ON DCL.LeadDate = DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
	   AND DCL.AffiliateShortCodeId = dasc.AffiliateShortCodeId
	   AND DCL.ProductName = dp.Name
	   AND DCL.ClientName = dc.Name
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAccepted = 1 Lead might be sold & accepted on a different date
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND DCL.AffiliateShortCodeId IS NULL
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId,
	   DCL.AffiliateShortCodeId,
	   ddis.DistributionPointName,
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.Name


------------ Set MTD Conversion Rate to 100 where leads were not sent out for this month but a sale was made -------------------
UPDATE 
	  #ClientEarnings
SET 
	  MTDConversionRate = 1.00
FROM 
	  #ClientEarnings dce WITH (NOLOCK)
WHERE 
	   MTDSales IS NOT NULL
	   AND (MTDLeadsSent = 0 AND MTDSales >= 1)

		
------------ Insert Previous 2 month's data Into Temp table (Leads Sent) ---------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   dpa.Title AS ParentAffiliate,
	   ddis.DistributionPointName,
	   dasc.Code,
	   dasc.[Description],
	   COUNT(DISTINCT dld.LeadId) AS MTDLeadsSent,
	   NULL AS MTDSales,
	   ddis.DistributionPointId,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName
INTO 
	   #ClientEarningsConversions
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= DATEADD(MONTH, -2, @StartDate) AND CONVERT(Date,dld.[Date]) <= DATEADD(MONTH, -1, @EndDate)
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.Name,
	   ddis.DistributionPointId,
	   dasc.AffiliateShortCodeId,
	   dpa.Title,
	   ddis.DistributionPointName


--------------- Set MTD Sales (2nd Previous Month) -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SoldDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
       AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 WHERE 
	   dld.IsSold = 1
	   AND dld.IsValid = 1
	   --AND dld.IsAccepted = 1 Lead might be sold & accepted on a different date
	   AND CONVERT(Date,dld.[Date]) >= DATEADD(MONTH, -2, @StartDate) AND CONVERT(Date,dld.[Date]) <= DATEADD(MONTH, -2, @EndDate)
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId,
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
) 
UPDATE 
	  #ClientEarningsConversions
SET 
	  MTDSales = ISNULL(CTE.Cnt, 0)
FROM 
	  #ClientEarningsConversions dcec
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dcec.DistributionPointId = CTE.DistributionPointId
	  AND dcec.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND dcec.LeadDate = CTE.SoldDate


--------------- Set MTD Sales (Previous Month) -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS SoldDate,
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 WHERE 
	   dld.IsSold = 1
	   AND dld.IsValid = 1
	   --AND dld.IsAccepted = 1 Lead might be sold & accepted on a different date
	   AND CONVERT(Date,dld.[Date]) >= DATEADD(MONTH, -1, @StartDate) AND CONVERT(Date,dld.[Date]) <= DATEADD(MONTH, -1, @EndDate)
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsDuplicate = 0
 GROUP BY 
	   dasc.AffiliateShortCodeId,
	   ddis.DistributionPointId,
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date])
) 
UPDATE 
	  #ClientEarningsConversions
SET 
	  MTDSales = ISNULL(CTE.Cnt, 0)
FROM 
	  #ClientEarningsConversions dcec
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dcec.DistributionPointId = CTE.DistributionPointId
	  AND dcec.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND dcec.LeadDate = CTE.SoldDate

/*
--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
			ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
			END AS AffiliateCost,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualAffCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
			ELSE SUM(ISNULL(ProjectedDistributionCost,0)) 
			END AS DistributionCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualDistCost,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   --AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   dld.ActualEarnings,
	   dld.ActualAffiliateCost,
	   ActualDistributionCost
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdLeadCost = CTE.AffiliateCost,
	  IsActualAffCost = CTE.IsActualAffCost,
	  MtdDistributionCost = CTE.DistributionCost,
	  IsActualDistCost = CTE.IsActualDistCost,
	  MTDEarnings = CTE.Earnings,
	  IsActualEarning = CTE.IsActualEarnings
FROM
	  #ClientEarnings ce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON ce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND ce.Code = CTE.Code
	  AND ce.LeadDate = CTE.LeadDate
	  AND ce.DistributionPointId = CTE.DistributionPointId
where cte.Earnings <> 0.00
or cte.AffiliateCost <> 0.00
or cte.DistributionCost <> 0.00
*/

--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   --AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   dld.ActualEarnings
)
UPDATE 
	  #ClientEarnings
SET 
	  MTDEarnings = CTE.Earnings,
	  IsActualEarning = CTE.IsActualEarnings
FROM
	  #ClientEarnings ce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON ce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND ce.Code = CTE.Code
	  AND ce.LeadDate = CTE.LeadDate
	  AND ce.DistributionPointId = CTE.DistributionPointId

--------------- Update Affiliate Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
			ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
			END AS AffiliateCost,
	   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualAffiliate
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   --AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   dld.ActualAffiliateCost
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdLeadCost = CTE.AffiliateCost,
	  IsActualAffCost = CTE.IsActualAffiliate
FROM
	  #ClientEarnings ce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON ce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND ce.Code = CTE.Code
	  AND ce.LeadDate = CTE.LeadDate
	  AND ce.DistributionPointId = CTE.DistributionPointId

--------------- Update Distribution Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]) AS LeadDate,
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN SUM(ISNULL(ActualDistributionCost,0)) 
			ELSE SUM(ISNULL(ProjectedDistributionCost,0)) 
			END AS DisCost,
	   CASE WHEN dld.ActualDistributionCost IS NOT NULL AND ActualDistributionCost <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualDistCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dp.ProductId = @Product
	   AND dau.UserId  IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dc.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND ddis.DistributionPointId IN (SELECT Value FROM dbo.FnSplit(@DistributionPoint,','))
	   AND dpa.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   --AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(YEAR, dld.[Date]) + ' ' + DATENAME(MONTH, dld.[Date]),
	   dasc.AffiliateShortCodeId,
	   dasc.Code,
	   ddis.DistributionPointId,
	   dld.ActualDistributionCost
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdDistributionCost = CTE.DisCost,
	  IsActualDistCost = CTE.IsActualDistCost
FROM
	  #ClientEarnings ce WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON ce.AffiliateShortCodeId = CTE.AffiliateShortCodeId
	  AND ce.Code = CTE.Code
	  AND ce.LeadDate = CTE.LeadDate
	  AND ce.DistributionPointId = CTE.DistributionPointId

------------ Set Profit -----------------
;WITH CTE AS
(
 SELECT 
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId,
	   SUM(ISNULL(MtdEarnings,0) - ISNULL(MtdLeadCost,0) - ISNULL(MtdDistributionCost,0)) AS Profit
 FROM 
	   #ClientEarnings WITH (NOLOCK)
 GROUP BY
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId   
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdProfit = CTE.Profit
FROM 
	  #ClientEarnings AS CE WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON CTE.LeadDate = CE.LeadDate
	  AND CTE.AffiliateShortCodeId = CE.AffiliateShortCodeId
	  AND CTE.DistributionPointId = CE.DistributionPointId

----------- Set EPL -------------
;WITH CTE AS
(
 SELECT 
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId,
	   CASE WHEN SUM(ISNULL(ExclMTDLeadsAccepted,0)) + ISNULL(NonExclMTDLeadsAccepted,0) = 0 THEN 0 
			ELSE (MtdEarnings / CAST(SUM(ISNULL(ExclMTDLeadsAccepted,0)) + SUM(ISNULL(NonExclMTDLeadsAccepted,0)) AS DECIMAL)) 
	   END AS EPL
 FROM 
	   #ClientEarnings WITH (NOLOCK)
 GROUP BY
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId,
	   MtdEarnings,
	   NonExclMTDLeadsAccepted   
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdEpl = CTE.EPL
FROM 
	  #ClientEarnings AS CE WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON CTE.LeadDate = CE.LeadDate
	  AND CTE.AffiliateShortCodeId = CE.AffiliateShortCodeId
	  AND CTE.DistributionPointId = CE.DistributionPointId


----------- Set CPL -------------
;WITH CTE AS
(
 SELECT 
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId,
	   CASE WHEN SUM(ISNULL(ExclMTDLeadsAccepted,0)) + ISNULL(NonExclMTDLeadsAccepted,0) = 0 THEN 0 
			ELSE SUM(ISNULL(MtdLeadCost,0) + ISNULL(MtdDistributionCost,0)) / CAST(SUM(ISNULL(ExclMTDLeadsAccepted,0)) + SUM(ISNULL(NonExclMTDLeadsAccepted,0)) AS DECIMAL) 
	   END AS CPL
 FROM 
	   #ClientEarnings WITH (NOLOCK)
 GROUP BY
	   LeadDate,
	   AffiliateShortCodeId,
	   DistributionPointId,
	   NonExclMTDLeadsAccepted   
)
UPDATE 
	  #ClientEarnings
SET 
	  MtdCpl = CTE.CPL
FROM 
	  #ClientEarnings AS CE WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON CTE.LeadDate = CE.LeadDate
	  AND CTE.AffiliateShortCodeId = CE.AffiliateShortCodeId
	  AND CTE.DistributionPointId = CE.DistributionPointId

---------------- Insert Final Records into Temp Table --------------------------
SELECT
	  Code AS AffiliateShortCode, 
	  DistributionPointName,
	  ProductName,
	  ClientName,
	  [Description] AS ShortCodeDescription, 
	  SUM(MTDLeadsSent) AS MTDLeadsSent, 
	  SUM(ISNULL(ExclMTDLeadsAccepted,0)) AS ExclMTDLeadsAccepted, 
	  SUM(ISNULL(NonExclMTDLeadsAccepted,0)) AS NonExclMTDLeadsAccepted,
	  SUM(MTDSales) AS MTDSales, 
	  CASE WHEN SUM(ISNULL(ExclMTDLeadsAccepted,0)) + ISNULL(NonExclMTDLeadsAccepted,0) = 0 THEN 0 
		   ELSE CONVERT(DECIMAL (5,2),CAST(SUM(MTDSales) AS DECIMAL) / CAST(SUM(ISNULL(ExclMTDLeadsAccepted,0)) + ISNULL(NonExclMTDLeadsAccepted,0) AS DECIMAL)) 
	  END AS MTDConversionRate,
	  CONVERT(DECIMAL(5,3), 0) AS TwoMonthConversionRate,
	  CONVERT(DECIMAL(5,3), 0) AS OneMonthConversionRate,
	  SUM(MTDEarnings) AS MTDEarnings,
	  SUM(ISNULL(MtdLeadCost,0)) AS MtdLeadCost,
	  SUM(ISNULL(MtdDistributionCost,0)) AS MtdDistCost,
	  SUM(ISNULL(MtdProfit,0)) AS MtdProfit,
	  MTDEpl,
	  MtdCpl,
	  DistributionPointId,
	  AffiliateShortCodeId,
	  IsActualAffCost,
	  IsActualDistCost,
	  IsActualEarning
INTO 
	  #ClientEarningsRptSelection
FROM
	  #ClientEarnings WITH (NOLOCK)
GROUP BY
	  Code,
	  DistributionPointName,
	  [Description], 
	  ProductName,
	  ClientName,
	  DistributionPointId,
	  AffiliateShortCodeId,
	  MTDEpl,
	  MtdCpl,
	  NonExclMTDLeadsAccepted,
	  IsActualAffCost,
	  IsActualDistCost,
	  IsActualEarning


------------ Set MTD Conversion Rate (2nd Previous Month) -------------------
;WITH CTE AS
(
 SELECT
	   dcec.AffiliateShortCodeId,
	   dcec.DistributionPointId,
	   CASE WHEN MTDLeadsSent = 0 AND MTDSales >= 1 THEN 1.00 ELSE CONVERT(DECIMAL (5,3),CAST(sum(MTDSales) AS DECIMAL) / CAST(sum(MTDLeadsSent) AS DECIMAL)) END AS ConversionCal 
 FROM 
	   #ClientEarningsConversions dcec WITH (NOLOCK)
 WHERE 
	   dcec.LeadDate >= DATEADD(MONTH, -2, @StartDate) AND dcec.LeadDate <= DATEADD(MONTH, -2, @EndDate)
 GROUP BY 
	   dcec.AffiliateShortCodeId,
	   dcec.DistributionPointId,
	   MTDLeadsSent,
	   MTDSales
) 
UPDATE 
	  #ClientEarningsRptSelection
SET 
	  TwoMonthConversionRate = CTE.ConversionCal
FROM 
	  #ClientEarningsRptSelection dcec
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dcec.DistributionPointId = CTE.DistributionPointId
	  AND dcec.AffiliateShortCodeId = CTE.AffiliateShortCodeId
WHERE 
	  CTE.ConversionCal IS NOT NULL


------------ Set MTD Conversion Rate (Previous Month) -------------------
;WITH CTE AS
(
 SELECT
	   dcec.AffiliateShortCodeId,
	   dcec.DistributionPointId,
	   CASE WHEN MTDLeadsSent = 0 AND MTDSales >= 1 THEN 1.00 ELSE CONVERT(DECIMAL (5,3),CAST(sum(MTDSales) AS DECIMAL) / CAST(sum(MTDLeadsSent) AS DECIMAL)) END AS ConversionCal
 FROM 
	   #ClientEarningsConversions dcec WITH (NOLOCK)
 WHERE 
	   dcec.LeadDate >= DATEADD(MONTH, -1, @StartDate) AND dcec.LeadDate <= DATEADD(MONTH, -1, @EndDate)
 GROUP BY 
	   dcec.AffiliateShortCodeId,
	   dcec.DistributionPointId,
	   	   MTDLeadsSent,
	   MTDSales
) 
UPDATE 
	  #ClientEarningsRptSelection
SET 
	  OneMonthConversionRate = CTE.ConversionCal
FROM 
	  #ClientEarningsRptSelection dcec
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dcec.DistributionPointId = CTE.DistributionPointId
	  AND dcec.AffiliateShortCodeId = CTE.AffiliateShortCodeId
WHERE 
	  CTE.ConversionCal IS NOT NULL


	  
-------------- Return records for reporting -----------------
SELECT
	   AffiliateShortCode,
	   DistributionPointName,
	   ProductName,
	   ClientName,
	   ShortCodeDescription,
	   MTDLeadsSent,
	   ExclMTDLeadsAccepted,
	   NonExclMTDLeadsAccepted,
	   MTDSales,
	   ISNULL(MTDConversionRate, 0) AS MTDConversionRate,
	   TwoMonthConversionRate,
	   OneMonthConversionRate,
	   MTDEarnings,
	   MtdLeadCost,
	   MtdDistCost,
	   MtdProfit,
	   MTDEpl,
	   MtdCpl,
	   DistributionPointId,
	   AffiliateShortCodeId,
	   IsActualAffCost,
	   IsActualDistCost,
	   IsActualEarning
FROM
	   #ClientEarningsRptSelection WITH (NOLOCK)
	  

RETURN


---------- Drop Client Earnings Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ClientEarnings') IS NOT NULL
DROP TABLE #ClientEarnings

---------- Drop Client Earnings Conversions Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ClientEarningsConversions') IS NOT NULL
DROP TABLE #ClientEarningsConversions

---------- Drop Client EarningsRpt Selection Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ClientEarningsRptSelection') IS NOT NULL
DROP TABLE #ClientEarningsRptSelection

IF  OBJECT_ID (N'tempdb..#ExclusiveLeads') IS NOT NULL
DROP TABLE #ExclusiveLeads

IF  OBJECT_ID (N'tempdb..#NonExclusiveLeads') IS NOT NULL
DROP TABLE #NonExclusiveLeads

END TRY



----------------- Insert Into Exception Table if theres an error --------------------
BEGIN CATCH

INSERT INTO
	   DataMartStaging.dbo.SsrsErrorException
( 
 SpName,
 ReportName,
 TenantParameter,
 Parameter1,
 Parameter2,
 Parameter3,
 Parameter4,
 Parameter5,
 Parameter6,
 Parameter7,
 Parameter8,
 ErrorDescription,
 LineNumber,
 InsertDateTime
)
SELECT 
	  ERROR_PROCEDURE(),
	  @ReportName,
	  @TenantID,
	  'Product: ' + CONVERT(NVARCHAR(MAX), @Product, 110),
	  'Client: ' + CONVERT(NVARCHAR(MAX), @Client, 110),
	  'StartDate: ' + CONVERT(NVARCHAR(MAX), @StartDate, 110),
	  'EndDate: ' + CONVERT(NVARCHAR(MAX), @EndDate, 110),
	  'AccountManager: ' + CONVERT(NVARCHAR(MAX), @AccountManager, 110),
	  'ParentAffiliate: ' + CONVERT(NVARCHAR(MAX), @ParentAffiliate, 110),
	  'DistributionPoint: ' + CONVERT(NVARCHAR(MAX), @DistributionPoint, 110),
	  NULL AS Parameter8,
	  ERROR_MESSAGE(),
	  ERROR_LINE(),
	  GETDATE()

------ Force the SQL Job to fail. The job would be shown as success if it had failed after catching the error	    
RAISERROR('Please refer to the SsrsErrorException table for the full error message',16,1)
		   
END CATCH

END