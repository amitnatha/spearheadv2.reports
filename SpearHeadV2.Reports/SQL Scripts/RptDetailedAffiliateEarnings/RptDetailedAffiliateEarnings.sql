USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptDetailedAffiliateEarnings]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptDetailedAffiliateEarnings]
GO


CREATE PROCEDURE [Reports].[RptDetailedAffiliateEarnings]
@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@AccountManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


----------------- Insert Leads Sent ---------------------
SELECT DISTINCT
	   CONVERT(DATE,dld.[Date], 100) AS LeadDate,
	   dasc.Code AS ShortCode,
	   dasc.[Description] AS ShortCodeDescription,
	   dp.Name AS Product,
	   dc.Name AS Client,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSent,
	   NULL AS LeadsAccepted,
	   NULL AS LeadsSold,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   CONVERT(DECIMAL(10,2), 0) AS EPL,
	   0 AS IsActualEarnings
INTO 
	   #DetailedAffiliateEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY
	   CONVERT(DATE,dld.[Date], 100),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.Name,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date]) AS AcceptedDate,
	   dasc.Code AS ShortCode,
	   dld.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK) 
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK) 
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   Lead dl WITH (NOLOCK) 
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK) 
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
INNER JOIN
       Tenant dt WITH (NOLOCK) 
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dasc.Code,
	   dld.DistributionPointId
) 
UPDATE 
	  #DetailedAffiliateEarnings
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #DetailedAffiliateEarnings dae WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dae.DistributionPointId = CTE.DistributionPointId
	  AND dae.ShortCode = CTE.ShortCode
	  AND dae.LeadDate = CTE.AcceptedDate


--------------- Insert Leads Accepted where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Accepted cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the accepted cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO #DetailedAffiliateEarnings
SELECT DISTINCT 
	   CONVERT(Date,dld.[Date], 100) AS AcceptedDate,
	   dasc.Code AS ShortCode,
	   dasc.[Description] AS ShortCodeDescription,
	   dp.Name AS Product,
	   dc.NAME AS Client,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName,
	   0 AS LeadsSent,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   0 AS LeadsSold,
	   0.00 AS ConversionRate,
	   0.00 AS Earnings,
	   0.00 AS EPL,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK) 
 INNER JOIN 
	   Product dp WITH (NOLOCK) 
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK) 
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK) 
	   ON ddis.DistributionPointId = dld.DistributionPointId
 INNER JOIN 
	   Lead dl WITH (NOLOCK) 
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Client dc WITH (NOLOCK) 
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK) 
	   ON dld.AffiliateManagerId = dau.UserId
 INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
 INNER JOIN
       Tenant dt WITH (NOLOCK) 
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #DetailedAffiliateEarnings dae WITH (NOLOCK)
	   ON dae.LeadDate = CONVERT(Date,dld.[Date])
	   AND dae.ShortCode = dasc.Code
	   AND dae.DistributionPointId = ddis.DistributionPointId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dae.LeadsAccepted IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.NAME,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName


--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date]) AS SalesDate,
	   dasc.Code AS ShortCode,
	   dld.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK) 
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK) 
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   Lead dl WITH (NOLOCK) 
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK) 
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
INNER JOIN
       Tenant dt WITH (NOLOCK) 
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dasc.Code,
	   dld.DistributionPointId
) 
UPDATE 
	  #DetailedAffiliateEarnings
SET 
	  LeadsSold = CTE.Cnt
FROM 
	  #DetailedAffiliateEarnings dae WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dae.DistributionPointId = CTE.DistributionPointId
	  AND dae.ShortCode = CTE.ShortCode
	  AND dae.LeadDate = CTE.SalesDate


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	       #DetailedAffiliateEarnings
 SELECT DISTINCT
	   CONVERT(Date,dld.[Date], 100) AS SoldDate,
	   dasc.Code AS ShortCode,
	   dasc.[Description] AS ShortCodeDescription,
	   dp.Name AS Product,
	   dc.NAME AS Client,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName,
	   0 AS LeadsSent,
	   0 AS LeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold, 
	   0.00 AS ConversionRate,
	   0.00 AS Earnings,
	   0.00 AS EPL,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK) 
 INNER JOIN 
	   Product dp WITH (NOLOCK) 
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK) 
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK) 
	   ON ddis.DistributionPointId = dld.DistributionPointId
 INNER JOIN 
	   Lead dl WITH (NOLOCK) 
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Client dc WITH (NOLOCK) 
	   ON dc.ClientId = dld.ClientId
 INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK) 
	   ON dld.AffiliateManagerId = dau.UserId
 INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
 INNER JOIN
       Tenant dt WITH (NOLOCK) 
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #DetailedAffiliateEarnings dae WITH (NOLOCK)
	   ON dae.LeadDate = CONVERT(Date,dld.[Date])
	   AND dae.ShortCode = dasc.Code
	   AND dae.DistributionPointId = ddis.DistributionPointId
 WHERE 
	   --dld.IsAcceptedByClient = 1
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dae.LeadsSold IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dc.NAME,
	   ddis.DistributionPointId,
	   ddis.DistributionPointName


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   CONVERT(DATE,dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode,
	   dld.DistributionPointId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK) 
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK) 
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN 
	   Lead dl WITH (NOLOCK) 
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK) 
	   ON dld.AffiliateManagerId = dau.UserId
 INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.ClientManagerId = dau2.UserId
 INNER JOIN
       Tenant dt WITH (NOLOCK) 
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
GROUP BY
	   CONVERT(DATE,dld.[Date]),
	   dasc.Code,
	   dld.DistributionPointId,
	   ActualEarnings
)
UPDATE 
	  #DetailedAffiliateEarnings
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #DetailedAffiliateEarnings dae WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = dae.LeadDate
	  AND CTE.ShortCode = dae.ShortCode
	  AND CTE.DistributionPointId = dae.DistributionPointId


--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ShortCode,
	   DistributionPointId,
	   CASE WHEN LeadsAccepted = 0 THEN 0.00 ELSE CONVERT(DECIMAL (4,3),CAST(SUM(LeadsSold) AS DECIMAL) / CAST(SUM(LeadsAccepted) AS DECIMAL)) END AS ConversionCal
 FROM
	   #DetailedAffiliateEarnings WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ShortCode,
	   DistributionPointId,
	   LeadsAccepted
) 
UPDATE 
	  #DetailedAffiliateEarnings
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #DetailedAffiliateEarnings dae
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dae.DistributionPointId = CTE.DistributionPointId
	  AND dae.ShortCode = CTE.ShortCode
	  AND dae.LeadDate = CTE.LeadDate
WHERE 
	  CTE.ConversionCal IS NOT NULL


--------------- Update EPL -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ShortCode,
	   DistributionPointId,
	   CASE WHEN LeadsSent = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsSent) END AS EPL -- Set EPL to 0 if there were no leads sent for a particular day
 FROM
	   #DetailedAffiliateEarnings WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ShortCode,
	   DistributionPointId,
	   LeadsSent
) 
UPDATE 
	  #DetailedAffiliateEarnings
SET 
	  EPL = CTE.EPL
FROM 
	  #DetailedAffiliateEarnings dae WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dae.DistributionPointId = CTE.DistributionPointId
	  AND dae.ShortCode = CTE.ShortCode
	  AND dae.LeadDate = CTE.LeadDate

--------------- Select records for the report -------------------
SELECT *
FROM 
	  #DetailedAffiliateEarnings WITH (NOLOCK)

RETURN

---------- Drop #DetailedAffiliateEarnings Temp table -------------------
IF  OBJECT_ID (N'tempdb..#DetailedAffiliateEarnings') IS NOT NULL
DROP TABLE #DetailedAffiliateEarnings


END