USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptOperationsProductSummary]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptOperationsProductSummary]
GO


CREATE PROCEDURE [Reports].[RptOperationsProductSummary]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@ParentAffiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-11,@StartDate) -- Set the start date to 6 months prior to the month selected


--------------- Insert Earnings --------------------------
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   dc.ClientId,
	   dc.Name AS Client,
	   ddp.DistributionPointId,
	   0 AS GroupingFlag,
	   ddp.DistributionPointName,
	   SUM(ISNULL(dld.ProjectedEarnings,0)) AS Earnings,
	   --CONVERT(DECIMAL(10,2), 0) AS SaleIncome,
	   --CONVERT(DECIMAL(10,2), 0) AS CancelledIncome,
	   --CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   0 AS UniqueLeadsReceived,
	   0 AS UniqueLeadsAccepted,
	   CONVERT(DECIMAL(10,2), 0) AS LeadCost,
	   0 AS DistributionCost,
	   CONVERT(DECIMAL(10,2), 0) AS CostPerLead,
	   CONVERT(DECIMAL(10,2), 0) AS EarningsPerLead,
	   CONVERT(DECIMAL(10,2), 0) AS ProfitPerLead,
	   CONVERT(DECIMAL(10,2), 0) AS TotalProfit
INTO
	   #OperationsProductSummary
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]),
	   dc.ClientId,
	   dc.Name,
	   ddp.DistributionPointId,
	   ddp.DistributionPointName


/*
--------------- Update Sale Income Used to Calculate Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime) AS LeadDate,
	   dc.ClientId,
	   ddp.DistributionPointId,
	   SUM(dld.SaleIncome) AS SaleIncome
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
GROUP BY
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime),
	   dc.ClientId,
	   ddp.DistributionPointId
)
UPDATE 
	  #OperationsProductSummary
SET 
	  SaleIncome = CTE.SaleIncome
FROM
	  #OperationsProductSummary ops WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = ops.LeadDate
	  AND CTE.ClientId = ops.ClientId
	  AND CTE.DistributionPointId = ops.DistributionPointId


--------------- Insert Sale Income Used to Calculate Earnings --------------------------
------ Insert Sale Income if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsProductSummary
SELECT DISTINCT
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime) AS LeadDate,
	   dc.ClientId,
	   dc.Name AS Client,
	   ddp.DistributionPointId,
	   0 AS GroupingFlag,
	   ddp.DistributionPointName,
	   0.00 AS LeadIncome,
	   SUM(dld.SaleIncome) AS SaleIncome,
	   0.00 AS CancelledIncome,
	   0.00 AS Earnings,
	   0 AS UniqueLeadsReceived,
	   0 AS UniqueLeadsAccepted,
	   0.00 AS LeadCost,
	   0 AS DistributionCost,
	   0.00 AS CostPerLead,
	   0.00 AS EarningsPerLead,
	   0.00 AS ProfitPerLead,
	   0.00 AS TotalProfit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProductSummary ops WITH (NOLOCK)
	   ON ops.LeadDate = DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime)
	   AND ops.ClientId = dc.ClientId
	   AND ops.DistributionPointId = ddp.DistributionPointId
WHERE 
	   CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
	   AND ops.SaleIncome IS NULL
GROUP BY
	   DATENAME(MONTH, dld.SoldDateTime) + ' ' + DATENAME(YEAR, dld.SoldDateTime),
	   dc.ClientId,
	   dc.Name,
	   ddp.DistributionPointId,
	   ddp.DistributionPointName


--------------- Update Cancelled Income Used to Calculate Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime) AS LeadDate,
	   dc.ClientId,
	   ddp.DistributionPointId,
	   SUM(dld.CanceledIncome) AS CancelledIncome
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.CanceledDateTime) >= @StartDate AND CONVERT(Date,dld.CanceledDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.CanceledIncome = 1
GROUP BY
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime),
	   dc.ClientId,
	   ddp.DistributionPointId
)
UPDATE 
	  #OperationsProductSummary
SET 
	  CancelledIncome = CTE.CancelledIncome
FROM
	  #OperationsProductSummary ops WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = ops.LeadDate
	  AND CTE.ClientId = ops.ClientId
	  AND CTE.DistributionPointId = ops.DistributionPointId


--------------- Insert Cancelled Income Used to Calculate Earnings --------------------------
------ Insert Cancelled Income if the lead date does not appear in the temp table ------
INSERT INTO
	   #OperationsProductSummary
SELECT DISTINCT
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime) AS LeadDate,
	   dc.ClientId,
	   dc.Name AS Client,
	   ddp.DistributionPointId,
	   0 AS GroupingFlag,
	   ddp.DistributionPointName,
	   0.00 AS LeadIncome,
	   0 AS SaleIncome,
	   SUM(dld.CanceledIncome) AS CancelledIncome,
	   0.00 AS Earnings,
	   0 AS UniqueLeadsReceived,
	   0 AS UniqueLeadsAccepted,
	   0.00 AS LeadCost,
	   0 AS DistributionCost,
	   0.00 AS CostPerLead,
	   0.00 AS EarningsPerLead,
	   0.00 AS ProfitPerLead,
	   0.00 AS TotalProfit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddp WITH (NOLOCK)
	   ON ddp.DistributionPointId = dld.DistributionPointId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProductSummary ops WITH (NOLOCK)
	   ON ops.LeadDate = DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime)
	   AND ops.ClientId = dc.ClientId
	   AND ops.DistributionPointId = ddp.DistributionPointId
WHERE 
	   CONVERT(Date,dld.CanceledDateTime) >= @StartDate AND CONVERT(Date,dld.CanceledDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.CanceledDateTime = 1
	   AND ops.CancelledIncome IS NULL
GROUP BY
	   DATENAME(MONTH, dld.CanceledDateTime) + ' ' + DATENAME(YEAR, dld.CanceledDateTime),
	   dc.ClientId,
	   dc.Name,
	   ddp.DistributionPointId,
	   ddp.DistributionPointName


--------------- Update Earnings ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   ClientId,
	   DistributionPointId,
	   SUM(LeadIncome + SaleIncome + CancelledIncome) AS Earnings
 FROM
	   #OperationsProductSummary WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   ClientId,
	   DistributionPointId
)
UPDATE 
	  #OperationsProductSummary
SET		
	  #OperationsProductSummary.Earnings = CTE.Earnings
FROM 
	  #OperationsProductSummary ops WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = ops.LeadDate
	  AND CTE.ClientId = ops.ClientId
	  AND CTE.DistributionPointId = ops.DistributionPointId
*/

--------- Delete Records where earnings is 0 -------------
---- Records where earnings is 0 does not get reported on -----
DELETE 
FROM #OperationsProductSummary
WHERE Earnings = 0.00


--------------- Insert Leads Received -------------------
----- Insert Leads received used for 2nd table -----
SELECT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived, 
	   0 AS LeadsAccepted,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   CONVERT(DECIMAL(10,2), 0) AS LeadCost,
	   CONVERT(DECIMAL(10,2), 0) AS DistributionCost, 
	   CONVERT(DECIMAL(10,2), 0) AS CPL,
	   CONVERT(DECIMAL(10,2), 0) AS EPL,
	   CONVERT(DECIMAL(10,2), 0) AS PPL,
	   CONVERT(DECIMAL(10,2), 0) AS TotalProfit
INTO #OperationsProductSummaryCal
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
 GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
) 
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opsc.LeadDate = CTE.LeadDate


--------------- Insert Leads Accepted -------------------
---- Insert Leads Accepted if the date is not in the temp tbl -------
INSERT INTO #OperationsProductSummaryCal
SELECT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted, 
	   0.00 AS Earnings,
	   0.00 AS LeadCost,
	   0.00 AS DistributionCost,
	   0.00 AS CPL,
	   0.00 AS EPL,
	   0.00 AS PPL,
	   0.00 AS TotalProfit
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProductSummaryCal opsc WITH (NOLOCK)
	   ON opsc.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND opsc.LeadsAccepted IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])


--------------- Update Lead Cost --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   SUM(ISNULL(dld.ProjectedAffiliateCost,0)) AS LeadCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
)
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  LeadCost = CTE.LeadCost
FROM
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opsc.LeadDate



--------------- Insert Lead Cost --------------------------
---- Insert Lead Cost if the date is not in the temp tbl ---
INSERT INTO #OperationsProductSummaryCal
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0.00 AS Earnings,
	   SUM(ISNULL(dld.ProjectedAffiliateCost,0)) AS LeadCost, 
	   0.00 AS DistributionCost,
	   0.00 AS CPL,
	   0.00 AS EPL,
	   0.00 AS PPL,
	   0.00 AS TotalProfit	   
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
--INNER JOIN 
--	   Product dp WITH (NOLOCK)
--	   ON dld.ProductId = dp.ProductId
--INNER JOIN 
--	   ParentAffiliate dpa WITH (NOLOCK)
--	   ON dpa.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProductSummaryCal opsc WITH (NOLOCK)
	   ON opsc.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND opsc.LeadCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])


--------------- Update Cost Per Lead -------------------
;WITH CTE AS
(
SELECT DISTINCT
	   LeadDate,
	   CASE WHEN SUM(LeadsReceived) = 0 THEN 0 ELSE SUM(LeadCost) / SUM(LeadsReceived) END AS CostPerLead
FROM
	 #OperationsProductSummaryCal WITH (NOLOCK)
GROUP BY
      LeadDate
)
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  CPL = CTE.CostPerLead
FROM
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opsc.LeadDate



--------------- Update Earnings -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   SUM(Earnings) AS Earnings
 FROM
	   #OperationsProductSummary WITH (NOLOCK)
 GROUP BY
       LeadDate
) 
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  Earnings = CTE.Earnings
FROM 
	  #OperationsProductSummaryCal opsc WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opsc.LeadDate = CTE.LeadDate


--------------- Update Earnings Per Lead -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   CASE WHEN SUM(LeadsReceived) = 0 THEN 0.00 ELSE SUM(Earnings) / SUM(LeadsReceived) END AS EPL -- Set EPL to 0 if there were no leads sent for a particular day
 FROM
	   #OperationsProductSummaryCal WITH (NOLOCK)
 GROUP BY
       LeadDate
) 
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  EPL = CTE.EPL
FROM 
	  #OperationsProductSummaryCal opsc WITH (NOLOCK) 
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON opsc.LeadDate = CTE.LeadDate


---------------- Update Distribution Costs ---------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   SUM(ISNULL(dld.ProjectedDistributionCost,0)) AS DistributionCost
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
)
UPDATE 
	  #OperationsProductSummaryCal
SET 
	  DistributionCost = CTE.DistributionCost
FROM
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opsc.LeadDate


--------------- Insert Distribution Cost --------------------------
---- Insert Distribution Cost if the date is not in the temp tbl ---
INSERT INTO #OperationsProductSummaryCal
SELECT DISTINCT
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date]) AS LeadDate,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0.00 AS Earnings,
	   0.00 AS LeadCost, 
	   SUM(ISNULL(dld.ProjectedDistributionCost,0)) AS DistributionCost,
	   0.00 AS CPL,
	   0.00 AS EPL,
	   0.00 AS PPL,
	   0.00 AS TotalProfit	   
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN
	   #OperationsProductSummaryCal opsc WITH (NOLOCK)
	   ON opsc.LeadDate = DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,',')) 
	   AND dt.OldTenantId = @TenantID
	   AND opsc.DistributionCost IS NULL
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(MONTH, dld.[Date]) + ' ' + DATENAME(YEAR, dld.[Date])


--------------- Update Total Profit ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   SUM(Earnings - LeadCost - DistributionCost) AS Profit
 FROM
	   #OperationsProductSummaryCal WITH (NOLOCK)
 GROUP BY
       LeadDate
)
UPDATE  
	  #OperationsProductSummaryCal
SET		
	  #OperationsProductSummaryCal.TotalProfit = CTE.Profit
FROM 
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opsc.LeadDate


--------------- Update Profit Per Lead ---------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   CASE WHEN LeadsReceived = 0 THEN 0 ELSE SUM(TotalProfit) / SUM(LeadsReceived) END AS PPL
 FROM
	   #OperationsProductSummaryCal WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   LeadsReceived
)
UPDATE 
	  #OperationsProductSummaryCal
SET		
	  #OperationsProductSummaryCal.PPL = CTE.PPL
FROM 
	  #OperationsProductSummaryCal opsc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = opsc.LeadDate


-------------- Update Grouping Used to Insert missing dates -----------------
;WITH CTE AS
(
 SELECT 
	    ClientId, 
		DistributionPointId,
		DENSE_RANK() OVER (ORDER BY ClientId,DistributionPointId) AS GroupFlag
 FROM
	    #OperationsProductSummary WITH (NOLOCK)
)
UPDATE 
	  #OperationsProductSummary
SET 
	  GroupingFlag = CTE.GroupFlag
FROM 
	  #OperationsProductSummary ops WITH (NOLOCK)
INNER JOIN
	  CTE 
	  ON CTE.ClientId = ops.ClientId
	  AND CTE.DistributionPointId = ops.DistributionPointId


------------- Insert missing dates into Temp Tbl ---------------
DECLARE @MinGrouping INT,
	    @MaxGrouping INT

SET @MinGrouping = (SELECT MIN(GroupingFlag) FROM #OperationsProductSummary)
SET @MaxGrouping = (SELECT MAX(GroupingFlag) FROM #OperationsProductSummary)

WHILE @MinGrouping <= @MaxGrouping

BEGIN

;WITH CTE AS
(
SELECT  
	  Month_Year_Name, 
	  @MinGrouping AS GroupingFlag
FROM 
	  Calendar cal WITH (NOLOCK)
WHERE
	  cal.Month_Year_Name NOT IN (
								  SELECT LeadDate	
								  FROM #OperationsProductSummary ops WITH (NOLOCK)
								  WHERE GroupingFlag = @MinGrouping
								 )
	  AND CONVERT(DATE,Calendar_Date) >= @StartDate AND CONVERT(DATE,Calendar_Date) <= @EndDate
)
INSERT INTO #OperationsProductSummary
SELECT DISTINCT
	   cte.Month_Year_Name, 
	   ops.ClientId,
	   ops.Client,
	   ops.DistributionPointId,
	   ops.GroupingFlag,
	   ops.DistributionPointName,
	   --0.00 AS LeadIncome,
	   --0.00 AS SaleIncome,
	   --0.00 AS CancelledIncome,
	   0.00 AS Earnings,
	   0 AS UniqueLeadsReceived,
	   0 AS UniqueLeadsAccepted,
	   0.00 AS LeadCost,
	   0 AS DistributionCost,
	   0.00 AS CostPerLead,
	   0.00 AS EarningsPerLead,
	   0.00 AS ProfitPerLead,
	   0.00 AS TotalProfit
FROM
	  CTE
INNER JOIN
	   #OperationsProductSummary ops WITH (NOLOCK)
	   ON CTE.GroupingFlag = ops.GroupingFlag
	   
SET @MinGrouping = (SELECT MIN(GroupingFlag) FROM #OperationsProductSummary WHERE @MinGrouping < GroupingFlag)

 
END


--------------- Select records for the report -------------------
;WITH CTE AS
( 
SELECT  -- Select records that only has earnings
	   LeadDate,
	   Client,
	   DistributionPointName,
	   0 AS TotalUniqueLeadsReceived,
	   0 AS TotalUniqueLeadsAccepted,
	   0 AS TotalAffiliateCost,
	   0 AS DistributionCost,
	   Earnings,
	   NULL AS CPL,
	   NULL AS EPL,
	   NULL AS PPL,
	   0 AS TotalProfit
FROM 
	   #OperationsProductSummary WITH (NOLOCK)
UNION ALL
SELECT -- Select records for the 2nd table
       LeadDate,
	   NULL AS Client,
	   NULL AS DistributionPointName,
	   LeadsReceived AS TotalUniqueLeadsReceived,
	   LeadsAccepted AS TotalUniqueLeadsAccepted,
	   LeadCost AS TotalAffiliateCost,
	   DistributionCost AS DistributionCost,
	   NULL AS Earnings,
	   CPL,
	   EPL,
	   PPL,
	   TotalProfit
FROM
	   #OperationsProductSummaryCal WITH (NOLOCK)
)
SELECT 
	LEFT(LeadDate, 3) + '-' + RIGHT(CAST(YEAR(LeadDate) as CHAR(4)), 2) AS LeadDate,
	Client,
	DistributionPointName,
	TotalUniqueLeadsReceived,
	TotalUniqueLeadsAccepted,
	TotalAffiliateCost,
	DistributionCost,
	Earnings,
	CPL,
	EPL,
	PPL,
	TotalProfit
FROM 
	CTE
ORDER BY
	YEAR(LeadDate), MONTH(LeadDate) ASC

RETURN

---------- Drop #OperationsProductSummary Temp table -------------------
IF  OBJECT_ID (N'tempdb..#OperationsProductSummary') IS NOT NULL
DROP TABLE #OperationsProductSummary

---------- Drop #OperationsProductSummaryCal Temp table -------------------
IF  OBJECT_ID (N'tempdb..#OperationsProductSummaryCal') IS NOT NULL
DROP TABLE #OperationsProductSummaryCal

END