USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptClientProductOverviewByChannel]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptClientProductOverviewByChannel]
GO


CREATE PROCEDURE [Reports].[RptClientProductOverviewByChannel]
@StartDate DATE,
@EndDate DATE,
@ParentAffiliate NVARCHAR(MAX),
@Affiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@Channel NVARCHAR(MAX),
@Client NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


----------------- Insert Leads Sent ---------------------
SELECT DISTINCT
	   CONVERT(DATE,dld.[Date], 100) AS LeadDate,
	   dcl.ClientId,
	   dcl.Name AS Client,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSent,
	   NULL AS LeadsAccepted,
	   NULL AS LeadsSold,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
INTO 
	   #ClientProductOverviewByChannel
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Client dcl WITH (NOLOCK)
	   ON dcl.ClientId = dld.ClientId
INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY
	   CONVERT(DATE,dld.[Date], 100),
	   dc.Name,
	   dcl.Name,
	   dc.ChannelId,
	   dcl.ClientId


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date], 100) AS AcceptedDate,
	   dld.ClientId,
	   dld.ChannelId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE,dld.[Date], 100),
	   dld.ClientId,
	   dld.ChannelId
) 
UPDATE 
	  #ClientProductOverviewByChannel
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ClientProductOverviewByChannel cpoc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON cpoc.ChannelId = CTE.ChannelId
	  AND cpoc.ClientId = CTE.ClientId
	  AND cpoc.LeadDate = CTE.AcceptedDate


--------------- Insert Leads Accepted where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Accepted cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the accepted cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO #ClientProductOverviewByChannel
 SELECT
	   CONVERT(Date,dld.[Date], 100) AS AcceptedDate,
	   dcl.ClientId,
	   dcl.Name AS Client,
	   0 AS LeadsSent,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted, 
	   NULL AS LeadsSold,
	   0.000 AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Client dcl WITH (NOLOCK)
	   ON dcl.ClientId = dld.ClientId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ClientProductOverviewByChannel cpoc WITH (NOLOCK)
	   ON cpoc.LeadDate = CONVERT(Date,dld.[Date], 100)
	   AND cpoc.ChannelId = dc.ChannelId
	   AND cpoc.ClientId = dcl.ClientId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND cpoc.LeadsAccepted IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE,dld.[Date], 100),
	   dcl.ClientId,
	   dcl.Name,
	   dc.ChannelId,
	   dc.Name



--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date], 100) AS SalesDate,
	   dld.ClientId,
	   dld.ChannelId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dld.ClientId,
	   dld.ChannelId
) 
UPDATE 
	  #ClientProductOverviewByChannel
SET 
	  LeadsSold = CTE.Cnt
FROM 
	  #ClientProductOverviewByChannel cpoc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON cpoc.ClientId = CTE.ClientId
	  AND cpoc.ChannelId = CTE.ChannelId
	  AND cpoc.LeadDate = CTE.SalesDate


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #ClientProductOverviewByChannel
 SELECT DISTINCT
	   CONVERT(Date,dld.[Date], 100) AS SoldDate,
	   dcl.ClientId,
	   dcl.Name AS Client,
	   0 AS LeadsSent,
	   0 AS LeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold, 
	   0.000 AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Client dcl WITH (NOLOCK)
	   ON dcl.ClientId = dld.ClientId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ClientProductOverviewByChannel cpoc WITH (NOLOCK)
	   ON cpoc.LeadDate = CONVERT(Date,dld.[Date], 100)
	   AND cpoc.ClientId = dcl.ClientId
	   AND cpoc.ChannelId = dc.ChannelId
 WHERE 
	   --dld.IsAcceptedByClient = 1
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND cpoc.LeadsSold IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dcl.ClientId,
	   dcl.Name,
	   dc.ChannelId,
	   dc.Name



----------- Insert Records into Final Tbl ------------------
SELECT
	  Client,
	  SUM(LeadsSent) AS LeadsSent,
	  SUM(LeadsAccepted) AS LeadsAccepted,
	  SUM(ISNULL(LeadsSold, 0)) AS LeadsSold,
	  ConversionRate,
	  Channel,
	  CONVERT(DECIMAL(10,2), 0) AS Earnings,
	  0 AS IsActualEarnings
INTO  #ClientProdOverviewByChannelRptSelection
FROM
      #ClientProductOverviewByChannel WITH (NOLOCK)
GROUP BY
	  Client,
	  Channel,
	  ConversionRate



--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
	   Client,
	   Channel,
	   CASE WHEN LeadsAccepted = 0 THEN 0.00 ELSE CONVERT(DECIMAL (4,3),CAST(SUM(LeadsSold) AS DECIMAL) / CAST(SUM(LeadsAccepted) AS DECIMAL)) END AS ConversionCal
 FROM
	   #ClientProdOverviewByChannelRptSelection WITH (NOLOCK)
 GROUP BY
       Client,
	   Channel,
	   LeadsAccepted
) 
UPDATE 
	  #ClientProdOverviewByChannelRptSelection
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #ClientProdOverviewByChannelRptSelection cpoc
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON cpoc.Client = CTE.Client
	  AND cpoc.Channel = CTE.Channel
WHERE 
	  CTE.ConversionCal IS NOT NULL



--------------- Update Earnings --------------------------
;WITH CTELeadIncome AS
(
SELECT DISTINCT
	   dcl.Name AS ClientName,
	   dc.Name AS ChannelName,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Client dcl WITH (NOLOCK)
	   ON dcl.ClientId = dld.ClientId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   --AND dld.IsAcceptedByClient = 1
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
GROUP BY
	   dcl.Name,
	   dc.Name,
	   ActualEarnings
)
UPDATE 
	  #ClientProdOverviewByChannelRptSelection
SET 
	  Earnings = cli.Earnings,
	  IsActualEarnings = cli.IsActualEarnings
FROM
	  #ClientProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
INNER JOIN 
	  CTELeadIncome cli WITH (NOLOCK)
	  ON cpo.Channel = cli.ChannelName
	  AND cpo.Client = cli.ClientName



----------- Select Records for the Report ------------------
SELECT
	  Client,
	  LeadsSent,
	  LeadsAccepted,
	  LeadsSold,
	  ConversionRate,
	  Channel,
	  Earnings,
	  IsActualEarnings
FROM
      #ClientProdOverviewByChannelRptSelection WITH (NOLOCK)
ORDER BY
	  Client ASC

RETURN

---------- Drop #ClientProductOverviewByChannel Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ClientProductOverviewByChannel') IS NOT NULL
DROP TABLE #ClientProductOverviewByChannel


---------- Drop ProdOverviewByChannelRptSelection Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ClientProdOverviewByChannelRptSelection') IS NOT NULL
DROP TABLE #ClientProdOverviewByChannelRptSelection


END