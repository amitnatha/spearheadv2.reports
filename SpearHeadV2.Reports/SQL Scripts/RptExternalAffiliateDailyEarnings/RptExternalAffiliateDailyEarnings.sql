USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptExternalAffiliateDailyEarnings]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptExternalAffiliateDailyEarnings]
GO


CREATE PROCEDURE [Reports].[RptExternalAffiliateDailyEarnings]
@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER,
@AffiliateGUID NVARCHAR(MAX)

AS BEGIN


----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS Sales,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   0 AS IsActualEarnings
INTO 
	   #ExternalAffiliateDailyEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dp.ProductId



--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateDailyEarnings
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ExternalAffiliateDailyEarnings eaml WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eaml.LeadDate = CTE.LeadDate
	  AND eaml.AffiliateShortCode = CTE.ShortCode
	  AND eaml.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #ExternalAffiliateDailyEarnings
 SELECT DISTINCT
	   CONVERT(DATE, dld.[Date]) AS LeadDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   0 AS Sales,
	   0.000 AS ConversionRate,
	   0.00 AS Earnings,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateDailyEarnings eade WITH (NOLOCK)
	   ON eade.LeadDate = CONVERT(DATE, dld.[Date])
	   AND eade.AffiliateShortCode = dasc.Code
	   AND eade.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND eade.LeadsAccepted IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(DATE, dld.[Date]) AS SalesDate,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateDailyEarnings
SET 
	  Sales = CTE.Cnt
FROM 
	  #ExternalAffiliateDailyEarnings eade WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eade.LeadDate = CTE.SalesDate
	  AND eade.AffiliateShortCode = CTE.ShortCode
	  AND eade.ProductId = CTE.ProductId


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #ExternalAffiliateDailyEarnings
 SELECT DISTINCT
	   CONVERT(DATE, dld.[Date]) AS SoldDate,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS Sales,  
	   0.00 AS ConversionRate,
	   0.00 AS Earnings,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateDailyEarnings eade WITH (NOLOCK)
	   ON eade.LeadDate = CONVERT(DATE, dld.[Date])
	   AND eade.AffiliateShortCode = dasc.Code
	   AND eade.ProductId = dp.ProductId
 WHERE 
	   --dld.IsAcceptedByClient = 1
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dp.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND eade.Sales IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   CONVERT(Date,dld.[Date]) AS LeadDate,
	   dasc.Code AS AffiliateShortCode,
	   dld.ProductId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   CONVERT(Date,dld.[Date]),
	   dasc.Code,
	   dld.ProductId,
	   ActualEarnings
)
UPDATE 
	  #ExternalAffiliateDailyEarnings
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #ExternalAffiliateDailyEarnings eade WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadDate = eade.LeadDate
	  AND CTE.AffiliateShortCode = eade.AffiliateShortCode
	  AND CTE.ProductId = eade.ProductId


--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   CASE WHEN LeadsAccepted = 0 THEN 0.00 ELSE CONVERT(DECIMAL (4,3),CAST(SUM(Sales) AS DECIMAL) / CAST(SUM(LeadsAccepted) AS DECIMAL)) END AS ConversionCal
 FROM
	   #ExternalAffiliateDailyEarnings WITH (NOLOCK)
 GROUP BY
       LeadDate,
	   AffiliateShortCode,
	   ProductId,
	   LeadsAccepted
) 
UPDATE 
	  #ExternalAffiliateDailyEarnings
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #ExternalAffiliateDailyEarnings eade
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eade.ProductId = CTE.ProductId
	  AND eade.AffiliateShortCode = CTE.AffiliateShortCode
	  AND eade.LeadDate = CTE.LeadDate
WHERE 
	  CTE.ConversionCal IS NOT NULL


--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadDate,
	   AffiliateShortCode,
	   [Description],
	   Product,
	   LeadsReceived,
	   LeadsAccepted,
	   Sales,
	   ConversionRate,
	   Earnings,
	   IsActualEarnings
FROM 
	   #ExternalAffiliateDailyEarnings WITH (NOLOCK)
ORDER BY
	   LeadDate DESC

RETURN

---------- Drop #ExternalAffiliateDailyEarnings Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ExternalAffiliateDailyEarnings') IS NOT NULL
DROP TABLE #ExternalAffiliateDailyEarnings


END