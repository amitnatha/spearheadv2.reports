USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptExternalAffiliateMonthlyEarnings]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptExternalAffiliateMonthlyEarnings]
GO


CREATE PROCEDURE [Reports].[RptExternalAffiliateMonthlyEarnings]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER,
@AffiliateGUID NVARCHAR(MAX)

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-5,@StartDate) -- Set the start date to 6 months prior to the month selected



----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS Sales,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   CONVERT(DECIMAL(10,2), 0) AS Earnings,
	   0 AS IsActualEarnings
INTO 
	   #ExternalAffiliateMonthlyEarnings
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dp.ProductId



--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateMonthlyEarnings
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eame.LeadYear = CTE.LeadYear
	  AND eame.LeadMonth = CTE.LeadMonth
	  AND eame.AffiliateShortCode = CTE.ShortCode
	  AND eame.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #ExternalAffiliateMonthlyEarnings
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   0 AS Sales,
	   0.000 AS ConversionRate,
	   0.00 AS Earnings,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
	   ON eame.LeadYear = DATENAME(YEAR, dld.[Date])
	   AND eame.LeadMonth = DATENAME(MONTH, dld.[Date])
	   AND eame.AffiliateShortCode = dasc.Code
	   AND eame.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND eame.LeadsAccepted IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateMonthlyEarnings
SET 
	  Sales = CTE.Cnt
FROM 
	  #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eame.LeadYear = CTE.LeadYear
	  AND eame.LeadMonth = CTE.LeadMonth
	  AND eame.AffiliateShortCode = CTE.ShortCode
	  AND eame.ProductId = CTE.ProductId


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #ExternalAffiliateMonthlyEarnings
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS Sales,  
	   0.00 AS ConversionRate,
	   0.00 AS Earnings,
	   0 AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
	   ON eame.LeadYear = DATENAME(YEAR, dld.[Date])
	   AND eame.LeadMonth = DATENAME(MONTH, dld.[Date])
	   AND eame.AffiliateShortCode = dasc.Code
	   AND eame.ProductId = dp.ProductId
 WHERE 
	   --dld.IsAcceptedByClient = 1
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND eame.Sales IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffiliateShortCode,
	   dld.ProductId,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dld.ProductId,
	   ActualEarnings
)
UPDATE 
	  #ExternalAffiliateMonthlyEarnings
SET 
	  Earnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadYear = eame.LeadYear
	  AND CTE.LeadMonth = eame.LeadMonth
	  AND CTE.AffiliateShortCode = eame.AffiliateShortCode
	  AND CTE.ProductId = eame.ProductId

----------- Insert Earnings if date is not in temp Table --------------
INSERT INTO 
	   #ExternalAffiliateMonthlyEarnings
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   0 AS LeadsAccepted,
	   0 AS Sales,  
	   0.00 AS ConversionRate,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
			ELSE SUM(ISNULL(ProjectedEarnings,0)) 
			END AS Earnings,
	   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
			ELSE 0 
			END AS IsActualEarnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateMonthlyEarnings eame WITH (NOLOCK)
	   ON eame.LeadYear = DATENAME(YEAR, dld.[Date])
	   AND eame.LeadMonth = DATENAME(MONTH, dld.[Date])
	   AND eame.AffiliateShortCode = dasc.Code
	   AND eame.ProductId = dp.ProductId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND eame.Earnings IS NULL
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dp.ProductId,
	   dp.Name,
	   dasc.[Description],
	   ActualEarnings


--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
       LeadYear,
	   LeadMonth,
	   AffiliateShortCode,
	   ProductId,
	   CASE WHEN LeadsAccepted = 0 THEN 0.00 
	   ELSE CONVERT(DECIMAL (5,3),CAST(SUM(Sales) AS DECIMAL) / CAST(SUM(LeadsAccepted) AS DECIMAL)) END AS ConversionCal
 FROM
	   #ExternalAffiliateMonthlyEarnings WITH (NOLOCK)
 GROUP BY
       LeadYear,
	   LeadMonth,
	   AffiliateShortCode,
	   ProductId,
	   LeadsAccepted,
	   Sales
) 
UPDATE 
	  #ExternalAffiliateMonthlyEarnings
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #ExternalAffiliateMonthlyEarnings eame
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eame.ProductId = CTE.ProductId
	  AND eame.AffiliateShortCode = CTE.AffiliateShortCode
	  AND eame.LeadYear = CTE.LeadYear
	  AND eame.LeadMonth = CTE.LeadMonth
WHERE 
	  CTE.ConversionCal IS NOT NULL


--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadYear,
	   LeadMonth,
	   AffiliateShortCode,
	   [Description],
	   Product,
	   LeadsReceived,
	   LeadsAccepted,
	   Sales,
	   ConversionRate,
	   Earnings,
	   IsActualEarnings
FROM 
	   #ExternalAffiliateMonthlyEarnings WITH (NOLOCK)


RETURN

---------- Drop #ExternalAffiliateMonthlyEarnings Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ExternalAffiliateMonthlyEarnings') IS NOT NULL
DROP TABLE #ExternalAffiliateMonthlyEarnings


END