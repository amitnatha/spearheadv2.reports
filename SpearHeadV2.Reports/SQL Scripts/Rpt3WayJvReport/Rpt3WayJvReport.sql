USE [DataMartV2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[Rpt3WayJvOwedTo3Way]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[Rpt3WayJvOwedTo3Way]
GO

CREATE PROCEDURE [Reports].[Rpt3WayJvOwedTo3Way]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@TenantID UNIQUEIDENTIFIER

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))



------------- Owed to 3Way Section -------------
----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS LeadsAccepted,
	   CONVERT(DECIMAL(10,2), 0) AS JagLeadEarnings,
	   0 AS IsActualEarnings,
	   CONVERT(DECIMAL(10,2), 0) AS ThreeWayCosts,
	   0 AS IsActualThreeWayCosts,
	   CONVERT(DECIMAL(10,2), 0) AS Profit,
	   CONVERT(DECIMAL(10,2), 0) AS OwedTo3Way
INTO 
	   #ThreeWayJvReport
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
	   ParentAffiliate AS DPA WITH (NOLOCK)
	   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
	   AND DPA.Title = '3Way Marketing'
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dp.ProductId



--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
	   ParentAffiliate AS DPA WITH (NOLOCK)
	   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND DPA.Title = '3Way Marketing'
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ThreeWayJvReport
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ThreeWayJvReport AS TWJV WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON TWJV.LeadYear = CTE.LeadYear
	  AND TWJV.LeadMonth = CTE.LeadMonth
	  AND TWJV.AffiliateShortCode = CTE.ShortCode
	  AND TWJV.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #ThreeWayJvReport
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   0 AS JagLeadEarnings,
	   0 AS IsActualEarnings,
	   0 AS ThreeWayCosts,
	   0 AS IsActualThreeWayCosts,
	   0 AS Profit,
	   0 AS OwedTo3Way
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 INNER JOIN
	   ParentAffiliate AS DPA WITH (NOLOCK)
	   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ThreeWayJvReport AS TJVR WITH (NOLOCK)
	   ON TJVR.LeadYear = DATENAME(YEAR, dld.[Date])
	   AND TJVR.LeadMonth = DATENAME(MONTH, dld.[Date])
	   AND TJVR.AffiliateShortCode = dasc.Code
	   AND TJVR.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND TJVR.LeadsAccepted IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate  
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND DPA.Title = '3Way Marketing'
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  SUM(Earnings) AS Earnings,
	  MAX(IsActualEarnings) AS IsActualEarnings
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dasc.Code AS AffiliateShortCode,
			   dld.ProductId,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
					ELSE SUM(ISNULL(ProjectedEarnings,0)) 
					END AS Earnings,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualEarnings
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN 
			   AffiliateShortCode dasc WITH (NOLOCK)
			   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN
			   ParentAffiliate AS DPA WITH (NOLOCK)
			   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DPA.Title = '3Way Marketing'
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   dasc.Code,
			   dld.ProductId,
			   ActualEarnings
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId
)
UPDATE 
	  #ThreeWayJvReport
SET 
	  JagLeadEarnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #ThreeWayJvReport AS TJVR WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.AffiliateShortCode = TJVR.AffiliateShortCode
	  AND CTE.ProductId = TJVR.ProductId


--------------- Insert Earnings --------------------------
INSERT INTO #ThreeWayJvReport
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  [Description],
	  ProductId,
	  Product,
	  0 AS LeadsReceived,
	  0 AS LeadsAccepted,
	  SUM(Earnings) AS JagLeadEarnings,
	  MAX(IsActualEarnings) AS IsActualEarnings,
	  0 AS ThreeWayCosts,
	  0 AS IsActualThreeWayCosts,
	  0 AS Profit,
	  0 AS OwedTo3Way
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dasc.Code AS AffiliateShortCode,
			   dasc.[Description],
			   dld.ProductId,
			   dp.Name AS Product,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
					ELSE SUM(ISNULL(ProjectedEarnings,0)) 
					END AS Earnings,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualEarnings
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN 
			   AffiliateShortCode dasc WITH (NOLOCK)
			   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN Product AS DP WITH (NOLOCK)
			   ON DP.ProductId = dld.ProductId
	     INNER JOIN
			   ParentAffiliate AS DPA WITH (NOLOCK)
			   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		 LEFT OUTER JOIN
			   #ThreeWayJvReport AS TJVR WITH (NOLOCK)
			  ON DATENAME(YEAR, dld.[Date]) = TJVR.LeadYear
			  AND DATENAME(MONTH, dld.[Date]) = TJVR.LeadMonth
		      AND TJVR.AffiliateShortCode = dasc.Code
		      AND TJVR.ProductId = dld.ProductId
		WHERE 
			  CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			  AND dt.OldTenantId = @TenantID
			  AND dld.IsValid = 1
			  AND dl.IsTest = 0
			  AND TJVR.JagLeadEarnings IS NULL
			  AND DPA.Title = '3Way Marketing'
		GROUP BY
			  DATENAME(YEAR, dld.[Date]),
			  DATENAME(MONTH, dld.[Date]),
			  dasc.Code,
			  dld.ProductId,
			  ActualEarnings,
			  dasc.[Description],
			  dp.Name
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  AffiliateShortCode,
	  [Description],
	  Product


--------------- Update 3Way Costs --------------------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  SUM(LeadCost) AS LeadCost,
	  MAX(IsActualLeadCost) AS IsActualLeadCost
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dasc.Code AS AffiliateShortCode,
			   dld.ProductId,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS LeadCost,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualLeadCost
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN 
			   AffiliateShortCode dasc WITH (NOLOCK)
			   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN
			   ParentAffiliate AS DPA WITH (NOLOCK)
		       ON DPA.ParentAffiliateId = dld.ParentAffiliateId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DPA.Title = '3Way Marketing'
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   dasc.Code,
			   dld.ProductId,
			   ActualAffiliateCost
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId
)
UPDATE 
	  #ThreeWayJvReport
SET 
	  ThreeWayCosts = CTE.LeadCost,
	  IsActualThreeWayCosts = CTE.IsActualLeadCost
FROM
	  #ThreeWayJvReport AS TJVR WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.AffiliateShortCode = TJVR.AffiliateShortCode
	  AND CTE.ProductId = TJVR.ProductId


--------------- Insert 3Way Costs --------------------------
INSERT INTO #ThreeWayJvReport
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  [Description],
	  ProductId,
	  Product,
	  0 AS LeadsReceived,
	  0 AS LeadsAccepted,
	  0 AS JagEarnings,
	  0 AS IsActualearnings,
	  SUM(LeadCost) AS LeadCost,
	  MAX(IsActualLeadCost) AS IsActualLeadCost,
	  0 AS Profit,
	  0 AS OwedTo3Way
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dasc.Code AS AffiliateShortCode,
			   dasc.[Description],
			   dld.ProductId,
			   dp.Name AS Product,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS LeadCost,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualLeadCost
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN 
			   AffiliateShortCode dasc WITH (NOLOCK)
			   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN Product AS DP WITH (NOLOCK)
			   ON DP.ProductId = dld.ProductId
	     INNER JOIN
			   ParentAffiliate AS DPA WITH (NOLOCK)
			   ON DPA.ParentAffiliateId = dld.ParentAffiliateId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		 LEFT OUTER JOIN
			   #ThreeWayJvReport AS TJVR WITH (NOLOCK)
			  ON DATENAME(YEAR, dld.[Date]) = TJVR.LeadYear
			  AND DATENAME(MONTH, dld.[Date]) = TJVR.LeadMonth
		      AND TJVR.AffiliateShortCode = dasc.Code
		      AND TJVR.ProductId = dld.ProductId
		WHERE 
			  CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			  AND dt.OldTenantId = @TenantID
			  AND dld.IsValid = 1
			  AND dl.IsTest = 0
			  AND TJVR.ThreeWayCosts IS NULL
			  AND DPA.Title = '3Way Marketing'
		GROUP BY
			  DATENAME(YEAR, dld.[Date]),
			  DATENAME(MONTH, dld.[Date]),
			  dasc.Code,
			  dld.ProductId,
			  ActualAffiliateCost,
			  dasc.[Description],
			  dp.Name
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  AffiliateShortCode,
	  [Description],
	  Product


-------- Delete records that does not have either earnigns or Lead Cost ----------
DELETE
FROM #ThreeWayJvReport
WHERE (JagLeadEarnings IS NULL OR JagLeadEarnings = 0.00) 
	  AND (ThreeWayCosts IS NULL OR ThreeWayCosts = 0.00)
	  AND LeadsAccepted > 0


--------------- Set Profit -----------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  SUM(ISNULL(JagLeadEarnings,0) - ISNULL(ThreeWayCosts,0)) AS Profit
FROM 
	  #ThreeWayJvReport WITH (NOLOCK)
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId
)
UPDATE 
	  #ThreeWayJvReport
SET 
	  Profit = CTE.Profit
FROM 
	  #ThreeWayJvReport AS TJVR WITH (NOLOCK)
	  INNER JOIN CTE
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.AffiliateShortCode = TJVR.AffiliateShortCode
	  AND CTE.ProductId = TJVR.ProductId


--------------- Set Owed To 3Way-----------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId,
	  SUM(ISNULL(Profit,0) / 2 + ISNULL(ThreeWayCosts,0)) AS OwedTo3Way
FROM 
	  #ThreeWayJvReport WITH (NOLOCK)
GROUP BY
	  LeadYear,
	  LeadMonth,
	  AffiliateShortCode,
	  ProductId
)
UPDATE 
	  #ThreeWayJvReport
SET 
	  OwedTo3Way = CTE.OwedTo3Way
FROM 
	  #ThreeWayJvReport AS TJVR WITH (NOLOCK)
	  INNER JOIN CTE
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.AffiliateShortCode = TJVR.AffiliateShortCode
	  AND CTE.ProductId = TJVR.ProductId


--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadYear,
	   LeadMonth,
	   AffiliateShortCode,
	   [Description] AS ShortCodeDescription,
	   Product,
	   LeadsReceived,
	   LeadsAccepted,
	   JagLeadEarnings,
	   IsActualEarnings,
	   ThreeWayCosts,
	   IsActualThreeWayCosts,
	   Profit,
	   OwedTo3Way
FROM 
	  #ThreeWayJvReport WITH (NOLOCK)

RETURN

---------- Drop #ThreeWayJvReport Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ThreeWayJvReport') IS NOT NULL
DROP TABLE #ThreeWayJvReport


END