USE [DataMartV2]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[Rpt3WayJvOwedBy3Way]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[Rpt3WayJvOwedBy3Way]
GO

CREATE PROCEDURE [Reports].[Rpt3WayJvOwedBy3Way]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@TenantID UNIQUEIDENTIFIER

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))


------------- Owed By 3Way Section -------------
----------------- Insert Leads Accepted ---------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dp.ProductId,
	   dp.Name AS Product,
	   dld.ClientId,
	   dc.Name AS ClientName,
	   dld.DistributionPointId,
	   DDP.DistributionPointName,
	   DCCI.ClientCampaignIdentifierId,
	   DCCI.Value AS CCID,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   0 AS ThreeWayLeadCnt,
	   0 AS LeadDiscrepency,
	   CONVERT(DECIMAL(10,2), 0) AS JagLeadEarnings,
	   0 AS IsActualEarnings,
	   0 AS ThreeWayLeadEarnings,
	   CONVERT(DECIMAL(10,2), 0) AS JagCost,
	   0 AS IsActualJagCost,
	   CONVERT(DECIMAL(10,2), 0) AS JagProfit,
	   0 AS ThreeWayProfit,
	   CONVERT(DECIMAL(10,2), 0) AS OwedBy3WayJAG,
	   CONVERT(DECIMAL(10,2), 0) AS OwedBy3Way3Way,
	   CONVERT(DECIMAL(10,2), 0) AS EPL
INTO 
	   #ThreeWayJvOwedBy3way
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN
	   Client AS DC WITH (NOLOCK)
	   ON DC.ClientId = dld.ClientId
INNER JOIN
	   DistributionPoint AS DDP WITH (NOLOCK)
	   ON DDP.DistributionPointId = dld.DistributionPointId
INNER JOIN
	   Integration AS DI WITH (NOLOCK)
	   ON DI.IntegrationId = dld.IntegrationId
INNER JOIN 
	   ClientCampaignIdentifier AS DCCI WITH (NOLOCK)
	   ON DCCI.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsAccepted = 1
	   AND DI.IntegrationDetail2 = 'SpearHead.InternalServices.ClientIntegrations.TenantJAG.ThreeWay'
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dp.Name,
	   dp.ProductId,
	   dld.ClientId,
	   dc.Name,
	   dld.DistributionPointId,
	   DDP.DistributionPointName,
	   DCCI.Value,
	   DCCI.ClientCampaignIdentifierId



--------------- Update Earnings --------------------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId,
	  SUM(Earnings) AS Earnings,
	  MAX(IsActualEarnings) AS IsActualEarnings
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dld.ProductId,
			   dld.ClientId,
			   dld.DistributionPointId,
			   dld.ClientCampaignIdentifierId,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
					ELSE SUM(ISNULL(ProjectedEarnings,0)) 
					END AS Earnings,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualEarnings
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN Integration AS DI WITH (NOLOCK)
			   ON DI.IntegrationId = dld.IntegrationId
		 INNER JOIN 
			   ClientCampaignIdentifier AS DCCI WITH (NOLOCK)
			   ON DCCI.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DI.IntegrationDetail2 = 'SpearHead.InternalServices.ClientIntegrations.TenantJAG.ThreeWay' 
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   ActualEarnings,
			   dld.ProductId,
			   dld.ClientId,
			   dld.DistributionPointId,
			   dld.ClientCampaignIdentifierId
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId
)
UPDATE 
	  #ThreeWayJvOwedBy3way
SET 
	  JagLeadEarnings = CTE.Earnings,
	  IsActualEarnings = CTE.IsActualEarnings
FROM
	  #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.DistributionPointId = TJVR.DistributionPointId
	  AND CTE.ProductId = TJVR.ProductId
	  AND CTE.ClientCampaignIdentifierId = TJVR.ClientCampaignIdentifierId
	  AND CTE.ClientId = TJVR.ClientId


--------------- Insert Earnings --------------------------
INSERT INTO #ThreeWayJvOwedBy3way
SELECT 
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  Product,
	  ClientId,
	  ClientName,
	  DistributionPointId,
	  DistributionPointName,
	  ClientCampaignIdentifierId,
	  CCID,
	  0 AS LeadsAccepted,
	  0 AS ThreeWayLeadCnt,
	  0 AS LeadDiscrepency,
	  SUM(Earnings) AS JagLeadEarnings,
	  MAX(IsActualEarnings) AS IsActualEarnings,
	  0 AS ThreeWayLeadEarnings,
	  0 AS JagCosts,
	  0 AS IsActualJagCosts,
	  0 AS JagProfit,
	  0 AS ThreeWayProfit,
	  0 AS OwedBy3WayJAG,
	  0 AS OwedBy3Way3Way,
	  0 AS EPL
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dld.ProductId,
			   DP.Name AS Product,
			   dld.ClientId,
			   DC.Name AS ClientName,
			   dld.DistributionPointId,
			   DDP.DistributionPointName,
			   dld.ClientCampaignIdentifierId,
			   DCCI.Value AS CCID,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN SUM(ISNULL(ActualEarnings,0)) 
					ELSE SUM(ISNULL(ProjectedEarnings,0)) 
					END AS Earnings,
			   CASE WHEN dld.ActualEarnings IS NOT NULL AND ActualEarnings <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualEarnings
		 FROM 
			   LeadDetail dld WITH (NOLOCK)
		 INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		 INNER JOIN 
			   AffiliateShortCode dasc WITH (NOLOCK)
			   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
		 INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		 INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		 INNER JOIN Integration AS DI WITH (NOLOCK)
			   ON DI.IntegrationId = dld.IntegrationId
		 INNER JOIN 
			   ClientCampaignIdentifier AS DCCI WITH (NOLOCK)
			   ON DCCI.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		 INNER JOIN Client AS DC WITH (NOLOCK)
			   ON DC.ClientId = dld.ClientId
	     INNER JOIN Product AS DP WITH (NOLOCK)
			   ON dp.ProductId = dld.ProductId
	     INNER JOIN DistributionPoint AS DDP WITH (NOLOCK)
			   ON DDP.DistributionPointId = dld.DistributionPointId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		 LEFT OUTER JOIN
			   #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
			  ON DATENAME(YEAR, dld.[Date]) = TJVR.LeadYear
			  AND DATENAME(MONTH, dld.[Date]) = TJVR.LeadMonth
		      AND TJVR.DistributionPointId = dld.DistributionPointId
		      AND TJVR.ProductId = dld.ProductId
			  AND TJVR.ClientId = dld.ClientId
			  AND TJVR.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DI.IntegrationDetail2 = 'SpearHead.InternalServices.ClientIntegrations.TenantJAG.ThreeWay' 
			   AND TJVR.JagLeadEarnings IS NULL
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   dld.ProductId,
			   DP.Name,
			   dld.ClientId,
			   DC.Name,
			   dld.DistributionPointId,
			   DDP.DistributionPointName,
			   dld.ClientCampaignIdentifierId,
			   DCCI.Value,
			   ActualEarnings
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  Product,
	  ClientId,
	  ClientName,
	  DistributionPointId,
	  DistributionPointName,
	  ClientCampaignIdentifierId,
	  CCID


--------------- Update JAG Costs --------------------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  ClientId,
	  ProductId,
	  ClientCampaignIdentifierId,
	  DistributionPointId,
	  SUM(LeadCost) AS LeadCost,
	  MAX(IsActualLeadCost) AS IsActualLeadCost
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dld.ClientId,
			   dld.ProductId,
			   dld.ClientCampaignIdentifierId,
			   dld.DistributionPointId,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS LeadCost,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualLeadCost
		FROM 
			   LeadDetail dld WITH (NOLOCK)
		INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		INNER JOIN Integration AS DI WITH (NOLOCK)
			   ON DI.IntegrationId = dld.IntegrationId
		INNER JOIN 
			   ClientCampaignIdentifier AS DCCI WITH (NOLOCK)
			   ON DCCI.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DI.IntegrationDetail2 = 'SpearHead.InternalServices.ClientIntegrations.TenantJAG.ThreeWay' 
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   dld.ClientId,
			   dld.ProductId,
			   dld.ClientCampaignIdentifierId,
			   dld.DistributionPointId,
			   ActualAffiliateCost
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ClientId,
	  ProductId,
	  ClientCampaignIdentifierId,
	  DistributionPointId
)
UPDATE 
	  #ThreeWayJvOwedBy3way
SET 
	  JagCost = CTE.LeadCost,
	  IsActualJagCost = CTE.IsActualLeadCost
FROM
	  #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.ProductId = TJVR.ProductId
	  AND CTE.ClientId = TJVR.ClientId
	  AND CTE.DistributionPointId = TJVR.DistributionPointId
	  AND CTE.ClientCampaignIdentifierId = TJVR.ClientCampaignIdentifierId


--------------- Insert JAG Costs --------------------------
INSERT INTO #ThreeWayJvOwedBy3way
SELECT 
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  Product,
	  ClientId,
	  ClientName,
	  DistributionPointId,
	  DistributionPointName,
	  ClientCampaignIdentifierId,
	  CCID,
	  0 AS LeadsAccepted,
	  0 AS ThreeWayLeadCnt,
	  0 AS LeadDiscrepency,
	  0 AS JagLeadEarnings,
	  0 AS IsActualEarnings,
	  0 AS ThreeWayLeadEarnings,
	  SUM(LeadCost) AS JagCosts,
	  MAX(IsActualLeadCost) AS IsActualJagCosts,
	  0 AS JagProfit,
	  0 AS ThreeWayProfit,
	  0 AS OwedBy3WayJAG,
	  0 AS OwedBy3Way3Way,
	  0 AS EPL
FROM (
		SELECT DISTINCT
			   DATENAME(YEAR, dld.[Date]) AS LeadYear,
			   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
			   dld.ClientId,
			   DC.Name AS ClientName,
			   dld.ProductId,
			   DP.Name AS product,
			   dld.ClientCampaignIdentifierId,
			   DCCI.Value AS CCID,
			   dld.DistributionPointId,
			   DDP.DistributionPointName,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN SUM(ISNULL(ActualAffiliateCost,0)) 
					ELSE SUM(ISNULL(ProjectedAffiliateCost,0)) 
					END AS LeadCost,
			   CASE WHEN dld.ActualAffiliateCost IS NOT NULL AND ActualAffiliateCost <> 0.00 THEN 1
					ELSE 0 
					END AS IsActualLeadCost
		FROM 
			   LeadDetail dld WITH (NOLOCK)
		INNER JOIN 
			   Lead dl WITH (NOLOCK)
			   ON dld.LeadId = dl.LeadId
			   AND dl.IsTest = 0
		INNER JOIN
			   Tenant dt WITH (NOLOCK)
			   ON dt.TenantId = dld.TenantId
		INNER JOIN 
			   Campaign dcam WITH (NOLOCK)
			   ON dcam.CampaignId = dld.CampaignId
		INNER JOIN Integration AS DI WITH (NOLOCK)
			   ON DI.IntegrationId = dld.IntegrationId
		INNER JOIN 
			   ClientCampaignIdentifier AS DCCI WITH (NOLOCK)
			   ON DCCI.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		 INNER JOIN Client AS DC WITH (NOLOCK)
			   ON DC.ClientId = dld.ClientId
	     INNER JOIN Product AS DP WITH (NOLOCK)
			   ON dp.ProductId = dld.ProductId
	     INNER JOIN DistributionPoint AS DDP WITH (NOLOCK)
			   ON DDP.DistributionPointId = dld.DistributionPointId
		 LEFT OUTER JOIN 
			   CampaignRule dcr WITH (NOLOCK)
			   ON dcr.CampaignRuleId = dld.CampaignRuleId
		 LEFT OUTER JOIN
			   #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
			   ON DATENAME(YEAR, dld.[Date]) = TJVR.LeadYear
			   AND DATENAME(MONTH, dld.[Date]) = TJVR.LeadMonth
		       AND TJVR.DistributionPointId = dld.DistributionPointId
		       AND TJVR.ProductId = dld.ProductId
			   AND TJVR.ClientId = dld.ClientId
			   AND TJVR.ClientCampaignIdentifierId = dld.ClientCampaignIdentifierId
		WHERE 
			   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
			   AND dt.OldTenantId = @TenantID
			   AND dld.IsValid = 1
			   AND dl.IsTest = 0
			   AND DI.IntegrationDetail2 = 'SpearHead.InternalServices.ClientIntegrations.TenantJAG.ThreeWay' 
			   AND TJVR.JagCost IS NULL
		GROUP BY
			   DATENAME(YEAR, dld.[Date]),
			   DATENAME(MONTH, dld.[Date]),
			   dld.ClientId,
			   DC.Name,
			   dld.ProductId,
			   DP.Name,
			   dld.ClientCampaignIdentifierId,
			   DCCI.Value,
			   dld.DistributionPointId,
			   DDP.DistributionPointName,
			   ActualAffiliateCost
)dervtbl
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ClientId,
	  product,
	  ProductId,
	  ClientName,
      ClientCampaignIdentifierId,
	  CCID,
	  DistributionPointId,
	  DistributionPointName


-------- Delete records that does not have either earnigns or Lead Cost ----------
DELETE
FROM #ThreeWayJvOwedBy3way
WHERE (JagLeadEarnings IS NULL OR JagLeadEarnings = 0.00) 
	  AND (JagCost IS NULL OR JagCost = 0.00)
	  AND LeadsAccepted > 0

--------------- Update EPL ---------------------
;WITH CteEPL AS 
(
 SELECT 
       LeadYear,
	   LeadMonth,
	   ProductId,
	   ClientId,
	   DistributionPointId,
	   ClientCampaignIdentifierId,
	   SUM(JagLeadEarnings / LeadsAccepted) AS EPL
 FROM
	   #ThreeWayJvOwedBy3way WITH (NOLOCK)
 GROUP BY
	   LeadYear,
	   LeadMonth,
	   ProductId,
	   ClientId,
	   DistributionPointId,
	   ClientCampaignIdentifierId
)
UPDATE 
	  #ThreeWayJvOwedBy3way
SET		
	  #ThreeWayJvOwedBy3way.EPL = CteEPL.EPL
FROM 
	  #ThreeWayJvOwedBy3way AS TJV WITH (NOLOCK)
INNER JOIN 
	  CteEPL WITH (NOLOCK)
	  ON CteEPL.LeadYear = TJV.LeadYear
	  AND CteEPL.LeadMonth = TJV.LeadMonth
	  AND CteEPL.DistributionPointId = TJV.DistributionPointId
	  AND CteEPL.ProductId = TJV.ProductId
	  AND CteEPL.ClientId = TJV.ClientId
	  AND CteEPL.ClientCampaignIdentifierId = TJV.ClientCampaignIdentifierId

--------------- Set Profit & Owed To 3Way-----------------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId,
	  SUM(ISNULL(JagLeadEarnings,0) - ISNULL(JagCost,0)) AS Profit
FROM 
	  #ThreeWayJvOwedBy3way WITH (NOLOCK)
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId
)
UPDATE 
	  #ThreeWayJvOwedBy3way
SET 
	  JagProfit = CTE.Profit
FROM 
	  #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
	  INNER JOIN CTE
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.ClientId = TJVR.ClientId
	  AND CTE.ProductId = TJVR.ProductId
	  AND CTE.ClientCampaignIdentifierId = TJVR.ClientCampaignIdentifierId
	  AND CTE.DistributionPointId = TJVR.DistributionPointId


-------- Set Owed by 3Way (JAG) -----------
;WITH CTE AS
(
SELECT 
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId,
	  SUM(ISNULL(JagProfit,0) / 2 + ISNULL(JagCost,0)) AS OwedBy3Way
FROM 
	  #ThreeWayJvOwedBy3way WITH (NOLOCK)
GROUP BY
	  LeadYear,
	  LeadMonth,
	  ProductId,
	  ClientId,
	  DistributionPointId,
	  ClientCampaignIdentifierId
)
UPDATE 
	  #ThreeWayJvOwedBy3way
SET 
	  OwedBy3WayJAG = CTE.OwedBy3Way
FROM 
	  #ThreeWayJvOwedBy3way AS TJVR WITH (NOLOCK)
	  INNER JOIN CTE
	  ON CTE.LeadYear = TJVR.LeadYear
	  AND CTE.LeadMonth = TJVR.LeadMonth
	  AND CTE.ClientId = TJVR.ClientId
	  AND CTE.ProductId = TJVR.ProductId
	  AND CTE.ClientCampaignIdentifierId = TJVR.ClientCampaignIdentifierId
	  AND CTE.DistributionPointId = TJVR.DistributionPointId

--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadYear,
	   LeadMonth,
	   Product,
	   ClientName,
	   DistributionPointName,
	   CCID,
	   LeadsAccepted,
	   ThreeWayLeadCnt,
	   LeadDiscrepency,
	   JagLeadEarnings,
	   IsActualEarnings,
	   ThreeWayLeadEarnings,
	   JagCost,
	   IsActualJagCost,
	   JagProfit,
	   ThreeWayProfit,
	   OwedBy3WayJAG,
	   OwedBy3Way3Way,
	   EPL
FROM 
	  #ThreeWayJvOwedBy3way WITH (NOLOCK)

RETURN

---------- Drop #ThreeWayJvOwedBy3way Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ThreeWayJvOwedBy3way') IS NOT NULL
DROP TABLE #ThreeWayJvOwedBy3way


END