USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptAffiliateSales]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptAffiliateSales]
GO


CREATE PROCEDURE [Reports].[RptAffiliateSales]
@Month NVARCHAR(10),
@Year NVARCHAR(4),
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@ParentAffiliate NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
set @StartDate = DATEADD(MONTH,-11,@StartDate)
SET @EndDate = CONVERT(DATE,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+12,0)))

SELECT DISTINCT
	   DATEPART(YEAR,dld.[Date]) AS LeadYear,
	   DATEPART(MONTH,dld.[Date]) AS LeadMonth,
	   da.Name AS Affiliate,
	   dpa.Title AS ParentAffiliate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold, 
	   dp.Name AS ProductName,
	   dau.Name AS AffiliateManager 
INTO 
	   #AffiliateSalesCnt
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau.UserId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateManagerId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
	   AND dld.IsValid = 1
GROUP BY
	   DATEPART(YEAR,dld.[Date]),
	   DATEPART(MONTH,dld.[Date]),
	   dp.Name,
	   dau.Name,
	   da.Name,
	   dpa.Title


------------- Insert Pivot records into temp table -------------------------
SELECT 
	  *
INTO  
	  #AffiliateSalesPivot
FROM (
      SELECT 
			LeadYear,
			DATENAME(MONTH,CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)) AS LeadMonth, 
			LeadsSold,
			Affiliate
	  FROM
		    #AffiliateSalesCnt WITH (NOLOCK)
) AS dervtbl
PIVOT
(
 SUM(LeadsSold)
 FOR LeadMonth IN (january, february, march, april, may, june, july, august, september, october, november, december)
)AS piv;



--------------- Insert NULL records in Pivot Table that does not have a record for a particular year -----------------
DECLARE @MinYear NVARCHAR (10),
@MaxYear NVARCHAR (10)

SELECT @MinYear = (Select MIN(LeadYear) FROM #AffiliateSalesPivot WITH (NOLOCK))
SELECT @MaxYear = (Select MAX(LeadYear) FROM #AffiliateSalesPivot WITH (NOLOCK))

SELECT @MinYear = CAST(@MinYear + '01' + '01' AS DATE)
SELECT @MaxYear = CAST(@MaxYear + '01' + '01' AS DATE)

;WITH CTE AS
(
SELECT
	  COUNT(*) OVER (partition by Affiliate) AS TotalRecords,
	  Affiliate,
	  LeadYear
FROM
	  #AffiliateSalesPivot WITH (NOLOCK) 
GROUP BY
	  Affiliate, 
	  leadyear
)
INSERT INTO	#AffiliateSalesPivot
(
 LeadYear,
 Affiliate
)
SELECT 
	  NewYear = CASE 
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MaxYear) >=1 
				THEN DATEPART(YEAR,@MaxYear)
					WHEN DATEDIFF(YEAR,CAST(CAST(cte.LeadYear AS VARCHAR(4)) + '01' + '01' AS DATE), @MinYear) <1 
				THEN DATEPART(YEAR,@MinYear)  
				END,
	  cte.Affiliate
FROM
	  #AffiliateSalesPivot piv WITH (NOLOCK)
INNER JOIN
	  CTE
	  ON cte.Affiliate  = piv.Affiliate
WHERE
	  CTE.TotalRecords < 2



------------------------ Insert data into Final Table using Unpivot ------------------------
SELECT
	  asp.LeadYear, 
	  asp.Affiliate, 
	  MONTH(dervtbl.LeadMonth + ' 1 2010') AS LeadMonth,
	  Leads = 
			 CASE dervtbl.LeadMonth
				  when 'January' then asp.January
				  when 'February' then asp.February
				  when 'March' then asp.March
				  when 'April' then asp.April
				  when 'May' then asp.May
				  when 'June' then asp.June
				  when 'July' then asp.July
				  when 'August' then asp.August
				  when 'September' then asp.September
				  when 'October' then asp.October
				  when 'November' then asp.November
				  when 'December' then asp.December
	        END
INTO #RptAffiliateSalesTbl
FROM (
	  SELECT LeadYear, Affiliate, january, february, march, april, may, june, july, august, september, october, november, december
	  FROM #AffiliateSalesPivot asp WITH (NOLOCK)
  ) asp
CROSS JOIN (
			  select 'January' union all
			  select 'February' union all
			  select 'March' union all
			  select 'April' union all
			  select 'May' union all
			  select 'June' union all
			  select 'July' union all
			  select 'August' union all
			  select 'September' union all
			  select 'October' union all
			  select 'November' union all
			  select 'December'
			) dervtbl (LeadMonth)


--------- Select Final Records for the report ------------------
SELECT
	  LeadYear,
	  LeadMonth,
	  Affiliate,
	  ISNULL(Leads, 0) AS LeadsReceived
FROM
	  #RptAffiliateSalesTbl WITH (NOLOCK)
WHERE 
	  CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) >= @StartDate
	  AND CAST(CAST(LeadYear AS VARCHAR(4)) + RIGHT('0' + CAST(LeadMonth AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE) <= @EndDate
ORDER BY 
	  Affiliate ASC

RETURN

---------- Drop Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateSalesCnt') AND type in (N'U'))
DROP TABLE #AffiliateSalesCnt

---------- Drop Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#AffiliateSalesPivot') AND type in (N'U'))
DROP TABLE #AffiliateSalesPivot

---------- Drop Temp table -------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#RptAffiliateSalesTbl') AND type in (N'U'))
DROP TABLE #RptAffiliateSalesTbl

END