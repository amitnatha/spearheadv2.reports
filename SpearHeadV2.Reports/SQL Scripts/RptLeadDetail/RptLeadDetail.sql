USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptLeadDetail]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptLeadDetail]
GO


CREATE PROCEDURE [Reports].[RptLeadDetail]
--@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@DistributionPoint NVARCHAR(MAX),
@TenantId NVARCHAR(MAX)

AS BEGIN

DECLARE @StartDate DATE

---- Set the start date to a week from when the user selects a date -----
SELECT @StartDate = DATEADD(WK,-1,@EndDate)


---------------- Select Records for Report ----------------------
SELECT
	  [Date] AS ReceivedDateTime,
	  CONVERT(VARCHAR,L.OldLeadId)  AS LeadId,
	  acode.[Description] AS AffiliateShortCodeDesctription,
	  DP.DistributionPointName,
	  P.Name AS ProductName,
	  FirstName,
	  LastName,
	  CellNumber,
	  Email,
	  LD.ClientReference,
	  CAST(IsAccepted AS INT) AS IsAcceptedByClient,
	  CAST(IsDuplicate AS INT) AS IsDuplicate,
	  CAST(ISNULL(IsSold, 0) AS INT) AS IsSold,
	  ISNULL(LDist.ClientLeadStatus, '') AS ClientStatus,
	  LS.LeadStatusDescription AS LeadStatus,
	  C.Name AS Campaign,
	  CR.Name AS CampaignRule
FROM
	  leaddetail LD WITH (NOLOCK)
INNER JOIN
	  lead L WITH (NOLOCK)
	  ON LD.LeadId = L.LeadId
INNER JOIN
	  affiliateshortcode acode WITH (NOLOCK)
	  ON LD.affiliateshortcodeId = acode.affiliateshortcodeId
INNER JOIN 
	  distributionpoint DP WITH (NOLOCK)
	  ON LD.distributionpointId = DP.distributionpointId
INNER JOIN 
	  Product P WITH (NOLOCK)
	  ON LD.ProductId = P.ProductId
INNER JOIN 
	  Campaign C WITH (NOLOCK)
	  ON LD.CampaignId = C.CampaignId
INNER JOIN 
	  LeadStatus LS WITH (NOLOCK)
	  ON LD.LeadStatusId = LS.LeadStatusId
INNER JOIN
	  Tenant dt WITH (NOLOCK)
	  ON ld.TenantId = dt.TenantId
LEFT JOIN 
	  CampaignRule CR WITH (NOLOCK)
	  ON CR.CampaignRuleId = LD.CampaignRuleID
LEFT JOIN 
	  LeadDisposition LDist WITH (NOLOCK)
	  ON LDist.LeadDispositionId = LD.LeadDispositionId
WHERE
	  CONVERT(DATE,ld.[Date]) >= @StartDate AND CONVERT(DATE,ld.[Date]) <= @EndDate
	  AND p.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	  AND dp.DistributionPointId = @DistributionPoint
	  AND istest = 0
	  AND dt.OldTenantId = @TenantID
ORDER BY
	  ld.[Date] DESC,
	  LeadId

RETURN

END


