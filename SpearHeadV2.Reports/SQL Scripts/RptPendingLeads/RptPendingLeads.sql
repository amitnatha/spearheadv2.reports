USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptPendingLeads]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptPendingLeads]
GO


CREATE PROCEDURE [Reports].[RptPendingLeads]
@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@AffiliateManager NVARCHAR(MAX),
@AccountManager NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN

------------- Insert All Pending Leads ------------------
SELECT DISTINCT
	   CONVERT(DATE,dld.[Date]) AS ReceivedDate,
	   dl.OldLeadId,
	   dp.Name AS ProductName,
	   dasc.[Description] AS AffiliateShortCode,
	   dcam.Name AS Campaign,
	   dc.Name AS ClientName,
	   dls.LeadStatusCategory,
	   dls.LeadStatusDescription,
	   dl.StatusReason AS LeadStatusReason	   
INTO 
	   #AllPendingLeads
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN 
	   ApplicationUser dau2 WITH (NOLOCK)
	   ON dld.AffiliateManagerId = dau2.UserId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId	= dld.AffiliateShortCodeId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
INNER JOIN LeadStatus dls WITH (NOLOCK)
	   ON dls.LeadStatusId = dld.LeadStatusId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dls.LeadStatusCategory <> 'Cancelled'
	   AND dls.LeadStatusCategory <> 'Success'
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dau.UserId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dau2.UserId IN (SELECT Value FROM dbo.FnSplit(@AffiliateManager,','))
	   AND dt.OldTenantId = @TenantID


------------- Insert Pending Leads that only failed once on a particular day ------------------
SELECT DISTINCT 
	   CONVERT(DATE, ReceivedDate, 100) AS ReceivedDate, 
	   OldLeadId, 
	   ProductName, 
	   AffiliateShortCode, 
	   Campaign,
	   LeadStatusCategory, 
	   LeadStatusDescription, 
	   LeadStatusReason,
       STUFF(
			  ( 
			   SELECT 
				    '; ' + ClientName
               FROM 
				    #AllPendingLeads apl2 WITH (NOLOCK)
               WHERE 
				    apl1.OldLeadId = apl2.OldLeadId
               FOR XML PATH(''),TYPE
			  )
              .value('.','NVARCHAR(MAX)'),1,2,''
		    ) AS ClientName
INTO #PendingLeadsRptSelection
FROM 
     #AllPendingLeads apl1 WITH (NOLOCK)


---------------- Select records for the Report --------------------
SELECT 
	  ReceivedDate,
	  CONVERT(VARCHAR,OldLeadId) AS OldLeadId,
	  ProductName,
	  AffiliateShortCode,
	  Campaign,
	  LeadStatusCategory,
	  LeadStatusDescription,
	  LeadStatusReason,
	  ClientName
FROM 
      #PendingLeadsRptSelection WITH (NOLOCK)
ORDER BY
	  ReceivedDate ASC

RETURN

---------- Drop All Pending Leads Temp table -------------------
IF  OBJECT_ID (N'tempdb..#AllPendingLeads') IS NOT NULL
DROP TABLE #AllPendingLeads

---------- Drop Pending Leads Rpt Selection Temp table -------------------
IF  OBJECT_ID (N'tempdb..#PendingLeadsRptSelection') IS NOT NULL
DROP TABLE #PendingLeadsRptSelection

END