USE [DataMartV2]
GO
	
------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptDailyLeadsToClient]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptDailyLeadsToClient]
GO


CREATE PROCEDURE [Reports].[RptDailyLeadsToClient]
@StartDate DATE,
@EndDate DATE,
@Product NVARCHAR(MAX),
@Client NVARCHAR(MAX),
@AccountManager NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER

AS BEGIN


---------------- Insert Leads Sent ----------------------
SELECT DISTINCT
	   ddis.DistributionPointId,
	   CONVERT(Date,dld.[Date]) AS LeadDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSent,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName,
	   ddis.DistributionPointName AS DistributionPoint,
	   0 AS Sales,
	   dau.Name AS AccountManager,
	   0 AS LeadsAccepted 
INTO 
	   #DailyLeadsToClientReport
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dp.Name,
	   ddis.DistributionPointName,
	   dc.Name,
	   dau.Name,
	   ddis.DistributionPointId


----------------- Update Sales ----------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date]) AS SoldDateTime, 
	   dld.DistributionPointId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON ddis.DistributionPointId = dld.DistributionPointId
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dt.OldTenantId = @TenantID
	   AND IsSold = 1
	   AND dld.IsValid = 1
	   --AND dld.IsAcceptedByClient = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date]), 
	   dld.DistributionPointId
) 
UPDATE 
	  #DailyLeadsToClientReport
SET 
	  Sales = ISNULL(CTE.Cnt, 0)
FROM 
	  #DailyLeadsToClientReport dltc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dltc.DistributionPointId = CTE.DistributionPointId
	  AND CONVERT(Date,CTE.SoldDateTime) = dltc.LeadDate


---------------- Insert Leads Sold where there is no lead date in the temp tbl ---------------------
;WITH CTE AS
(
SELECT DISTINCT
	   ddis.DistributionPointId,
	   CONVERT(Date,dld.[Date]) AS SoldDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #DailyLeadsToClientReport dlc WITH (NOLOCK)
	   ON CONVERT(Date,dld.[Date]) = dlc.LeadDate
	   AND dlc.ProductName = dp.Name
	   AND dlc.ClientName = dc.Name
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
	   AND dlc.Sales IS NULL
	   AND dld.IsValid = 1
GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dp.Name,
	   dc.Name,
	   ddis.DistributionPointId
)	
INSERT INTO #DailyLeadsToClientReport
SELECT DISTINCT 
	   dlc.DistributionPointId,
	   CTE.SoldDate,
	   0 AS LeadsSent,
	   dlc.ProductName,
	   dlc.ClientName,
	   dlc.DistributionPoint,
	   CTE.LeadsSold AS Sales,
	   dlc.AccountManager,
	   0 AS LeadsAccepted	
FROM
	   #DailyLeadsToClientReport dlc WITH (NOLOCK)
INNER JOIN 
	   CTE WITH (NOLOCK)
	   ON dlc.DistributionPointId = CTE.DistributionPointId
	   AND dlc.ProductName = CTE.ProductName
	   AND dlc.ClientName = CTE.ClientName


---------------- Update Leads Accepted ---------------------
;WITH CTE AS
(
SELECT DISTINCT
	   ddis.DistributionPointId,
	   CONVERT(Date,dld.[Date]) AS AcceptedDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName,
	   dau.Name AS AccountManager
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAccepted = 1
	   AND dld.IsValid = 1
GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dp.Name,
	   dc.Name,
	   dau.Name,
	   ddis.DistributionPointId,
	   dau.Name
)	
UPDATE 
	  #DailyLeadsToClientReport
SET 
	  LeadsAccepted = CTE.LeadsAccepted
FROM 
	  #DailyLeadsToClientReport dlc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON dlc.DistributionPointId = CTE.DistributionPointId
	  AND dlc.LeadDate = CTE.AcceptedDate
	  AND dlc.ProductName = CTE.ProductName
	  AND dlc.ClientName = CTE.ClientName
	  And dlc.AccountManager = CTE.AccountManager


---------------- Insert Leads Accepted where there is no lead date in the temp tbl ---------------------
;WITH CTE AS
(
SELECT DISTINCT
	   ddis.DistributionPointId,
	   CONVERT(Date,dld.[Date]) AS AcceptedDate,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted,
	   dp.Name AS ProductName,
	   dc.Name AS ClientName
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dl.LeadId = dld.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Client dc WITH (NOLOCK)
	   ON dc.ClientId = dld.ClientId
INNER JOIN 
	   DistributionPoint ddis WITH (NOLOCK)
	   ON dld.DistributionPointId = ddis.DistributionPointId
INNER JOIN 
	   ApplicationUser dau WITH (NOLOCK)
	   ON dld.ClientManagerId = dau.UserId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
LEFT OUTER JOIN 
	   #DailyLeadsToClientReport dlc WITH (NOLOCK)
	   ON CONVERT(Date,dld.[Date]) = dlc.LeadDate
	   AND dlc.ProductName = dp.Name
	   AND dlc.ClientName = dc.Name
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))
	   AND dld.ClientId IN (SELECT Value FROM dbo.FnSplit(@Client,','))
	   AND dld.ClientManagerId IN (SELECT Value FROM dbo.FnSplit(@AccountManager,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAccepted = 1
	   AND dlc.LeadsAccepted IS NULL
	   AND dld.IsValid = 1
GROUP BY 
	   CONVERT(Date,dld.[Date]),
	   dp.Name,
	   dc.Name,
	   dau.Name,
	   ddis.DistributionPointId
)	
INSERT INTO #DailyLeadsToClientReport
SELECT DISTINCT 
	   dlc.DistributionPointId,
	   CTE.AcceptedDate,
	   0 AS LeadsSent,
	   dlc.ProductName,
	   dlc.ClientName,
	   dlc.DistributionPoint,
	   0 AS Sales,
	   dlc.AccountManager,
	   CTE.LeadsAccepted	
FROM
	   #DailyLeadsToClientReport dlc WITH (NOLOCK)
INNER JOIN 
	   CTE WITH (NOLOCK)
	   ON dlc.DistributionPointId = CTE.DistributionPointId
	   AND dlc.ProductName = CTE.ProductName
	   AND dlc.ClientName = CTE.ClientName


------------- Select Records for Report --------------------
SELECT CONVERT(NVARCHAR, LeadDate, 103) AS LeadDate,
	   LeadsSent,
	   LeadsAccepted,
	   ProductName,
	   ClientName,
	   DistributionPoint,
	   Sales,
	   AccountManager
FROM 
	   #DailyLeadsToClientReport WITH (NOLOCK)
ORDER BY 
	   CONVERT(DATETIME, LeadDate, 103) ASC

RETURN

------------ Drop DailyLeads To ClientReport Temp Table ------------------
IF  OBJECT_ID (N'tempdb..#DailyLeadsToClientReport') IS NOT NULL
DROP TABLE #DailyLeadsToClientReport

END


