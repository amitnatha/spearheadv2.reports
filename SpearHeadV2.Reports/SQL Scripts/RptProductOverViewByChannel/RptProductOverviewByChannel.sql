USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptProductOverviewByChannel]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptProductOverviewByChannel]
GO


CREATE PROCEDURE [Reports].[RptProductOverviewByChannel]
@StartDate DATE,
@EndDate DATE,
@ParentAffiliate NVARCHAR(MAX),
@Affiliate NVARCHAR(MAX),
@Product NVARCHAR(MAX),
@Channel NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER


AS BEGIN


----------------- Insert Leads Sent ---------------------
SELECT DISTINCT
	   CONVERT(DATE,dld.[Date], 100) AS LeadDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSent,
	   NULL AS LeadsAccepted,
	   NULL AS LeadsSold,
	   CONVERT(DECIMAL(10,3), 0) AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
INTO 
	   #ProductOverviewByChannel
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsSubmitted = 1
	   AND dld.IsValid = 1
GROUP BY
	   CONVERT(DATE,dld.[Date], 100),
	   dp.Name,
	   dc.Name,
	   dp.ProductId,
	   dc.ChannelId


--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date], 100) AS AcceptedDate,
	   dld.ProductId,
	   dld.ChannelId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE,dld.[Date], 100),
	   dld.ProductId,
	   dld.ChannelId
) 
UPDATE 
	  #ProductOverviewByChannel
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ProductOverviewByChannel poc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON poc.ChannelId = CTE.ChannelId
	  AND poc.ProductId = CTE.ProductId
	  AND poc.LeadDate = CTE.AcceptedDate


--------------- Insert Leads Accepted where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Accepted cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the accepted cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO #ProductOverviewByChannel
 SELECT
	   CONVERT(Date,dld.[Date], 100) AS AcceptedDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsSent,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted, 
	   NULL AS LeadsSold,
	   0.000 AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ProductOverviewByChannel poc WITH (NOLOCK)
	   ON poc.LeadDate = CONVERT(Date,dld.[Date], 100)
	   AND poc.ChannelId = dc.ChannelId
	   AND poc.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND poc.LeadsAccepted IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(DATE,dld.[Date], 100),
	   dp.ProductId,
	   dc.ChannelId,
	   dp.Name,
	   dc.Name



--------------- Set Leads Sold -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   CONVERT(Date,dld.[Date], 100) AS SalesDate,
	   dld.ProductId,
	   dld.ChannelId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsSold = 1
	   --AND dld.IsAcceptedByClient = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dld.ProductId,
	   dld.ChannelId
) 
UPDATE 
	  #ProductOverviewByChannel
SET 
	  LeadsSold = CTE.Cnt
FROM 
	  #ProductOverviewByChannel poc WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON poc.ProductId = CTE.ProductId
	  AND poc.ChannelId = CTE.ChannelId
	  AND poc.LeadDate = CTE.SalesDate


--------------- Insert Leads Sold where leads were not sent on a particular day -------------------
/* /////////// Insert Leads Sold cnts where there is no sale date in the temp table. \\\\\\\\\\\\\\\\\\\\\
			   For instance if there were no leads sent on a particular day & a 
			   sale was made on that same day, the sale cnt would not be included
			   as the record would fall away when joining on Lead Date
*/

INSERT INTO 
	   #ProductOverviewByChannel
 SELECT DISTINCT
	   CONVERT(Date,dld.[Date], 100) AS SoldDate,
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsSent,
	   0 AS LeadsAccepted,
	   COUNT(DISTINCT dld.LeadId) AS LeadsSold, 
	   0.000 AS ConversionRate,
	   dc.ChannelId,
	   dc.Name AS Channel
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ProductOverviewByChannel poc WITH (NOLOCK)
	   ON poc.LeadDate = CONVERT(Date,dld.[Date], 100)
	   AND poc.ProductId = dp.ProductId
	   AND poc.ChannelId = dc.ChannelId
 WHERE 
	   --dld.IsAcceptedByClient = 1
	   dld.IsSold = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND poc.LeadsSold IS NULL
	   AND dld.IsValid = 1
 GROUP BY 
	   CONVERT(Date,dld.[Date], 100),
	   dp.ProductId,
	   dc.ChannelId,
	   dp.Name,
	   dc.Name

----------- Insert Records into Final Tbl ------------------
SELECT
	  Product,
	  SUM(LeadsSent) AS LeadsSent,
	  SUM(LeadsAccepted) AS LeadsAccepted,
	  SUM(ISNULL(LeadsSold, 0)) AS LeadsSold,
	  ConversionRate,
	  Channel,
	  CONVERT(DECIMAL(10,2), 0) AS Earnings
INTO  #ProdOverviewByChannelRptSelection
FROM
      #ProductOverviewByChannel WITH (NOLOCK)
GROUP BY
	  Product,
	  Channel,
	  ConversionRate

--------------- Update Conversion Rate -------------------
;WITH CTE AS 
(
 SELECT 
	   Product,
	   Channel,
	   CASE WHEN LeadsSent = 0 THEN 1.00 ELSE CONVERT(DECIMAL (4,3),CAST(SUM(LeadsSold) AS DECIMAL) / CAST(SUM(LeadsSent) AS DECIMAL)) END AS ConversionCal
 FROM
	   #ProdOverviewByChannelRptSelection WITH (NOLOCK)
 GROUP BY
       Product,
	   Channel,
	   LeadsSent
) 
UPDATE 
	  #ProdOverviewByChannelRptSelection
SET 
	  ConversionRate = CTE.ConversionCal
FROM 
	  #ProdOverviewByChannelRptSelection poc
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON poc.Product = CTE.Product
	  AND poc.Channel = CTE.Channel
WHERE 
	  CTE.ConversionCal IS NOT NULL



--------------- Update Earnings --------------------------
;WITH CTELeadIncome AS
(
SELECT DISTINCT
	   dp.Name AS Product,
	   dc.Name AS Channel,
	   SUM(ISNULL(dld.ProjectedEarnings,0)) AS Earnings
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND dld.AffiliateId IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dld.ChannelId IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dld.ParentAffiliateId IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   --AND dld.IsAcceptedByClient = 1
	   AND dld.IsValid = 1
GROUP BY
	   dp.Name,
	   dc.Name
)
UPDATE 
	  #ProdOverviewByChannelRptSelection
SET 
	  Earnings = cli.Earnings
FROM
	  #ProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
INNER JOIN 
	  CTELeadIncome cli WITH (NOLOCK)
	  ON cpo.Channel = cli.Channel
	  AND cpo.Product = cli.Product

/*
--------------- Update Sale Income (Used to calculate Earnings) --------------------------
;WITH CTESaleIncome AS
(
SELECT DISTINCT
	   dp.Name AS Product,
	   dc.Name AS Channel,
	   SUM(dld.SaleIncome) AS SaleIncome
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
WHERE 
	   CONVERT(Date,dld.SoldDateTime) >= @StartDate AND CONVERT(Date,dld.SoldDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND da.Name IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dc.Name IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsSold = 1
GROUP BY
	   dp.Name,
	   dc.Name
)
UPDATE 
	  #ProdOverviewByChannelRptSelection
SET 
	  SaleIncome = csi.SaleIncome
FROM
	  #ProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
INNER JOIN 
	  CTESaleIncome csi WITH (NOLOCK)
	  ON cpo.Channel = csi.Channel
	  AND cpo.Product = csi.Product


--------------- Update Cancelled Income (Used to calculate Earnings) --------------------------
;WITH CTESaleIncome AS
(
SELECT DISTINCT
	   dp.Name AS Product,
	   dc.Name AS Channel,
	   SUM(dld.CanceledIncome) AS CanceledIncome
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   ParentAffiliate dpa WITH (NOLOCK)
	   ON dpa.OldParentAffiliateId = da.ParentAffiliateId
 INNER JOIN 
	   Channel dc WITH (NOLOCK)
	   ON dc.ChannelId = dld.ChannelId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
WHERE 
	   CONVERT(Date,dld.CanceledDateTime) >= @StartDate AND CONVERT(Date,dld.CanceledDateTime) <= @EndDate
	   AND dp.Name IN (SELECT Value FROM dbo.FnSplit(@Product,',')) 
	   AND da.Name IN (SELECT Value FROM dbo.FnSplit(@Affiliate,','))
	   AND dc.Name IN (SELECT Value FROM dbo.FnSplit(@Channel,','))
	   AND dpa.Title IN (SELECT Value FROM dbo.FnSplit(@ParentAffiliate,','))
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsAcceptedByClient = 1
	   AND dld.IsCanceled = 1
GROUP BY
	   dp.Name,
	   dc.Name
)
UPDATE 
	  #ProdOverviewByChannelRptSelection
SET 
	  CancelledIncome = csi.CanceledIncome
FROM
	  #ProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
INNER JOIN 
	  CTESaleIncome csi WITH (NOLOCK)
	  ON cpo.Channel = csi.Channel
	  AND cpo.Product = csi.Product


------------- Set MTD Earnings (Final Table) -----------------------
;WITH CteEarnings AS
(
SELECT 
	  cpo.Product, 
	  cpo.Channel,
	  SUM(leadIncome + SaleIncome + CancelledIncome) AS Earnings
FROM 
	  #ProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
GROUP BY 
	  cpo.Product, 
	  cpo.Channel
)
UPDATE 
	  #ProdOverviewByChannelRptSelection
SET 
	  Earnings = ce.Earnings
FROM 
	  #ProdOverviewByChannelRptSelection cpo WITH (NOLOCK)
	  INNER JOIN CteEarnings ce
	  ON ce.Product = cpo.Product
	  AND ce.Channel = cpo.Channel
*/

----------- Select Records for the Report ------------------
SELECT
	  Product,
	  LeadsSent,
	  LeadsAccepted,
	  LeadsSold,
	  ConversionRate,
	  Channel,
	  Earnings
FROM
      #ProdOverviewByChannelRptSelection WITH (NOLOCK)
ORDER BY
	  Product ASC

RETURN

---------- Drop #ProductOverviewByChannel Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ProductOverviewByChannel') IS NOT NULL
DROP TABLE #ProductOverviewByChannel


---------- Drop #ProdOverviewByChannelRptSelection Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ProdOverviewByChannelRptSelection') IS NOT NULL
DROP TABLE #ProdOverviewByChannelRptSelection


END