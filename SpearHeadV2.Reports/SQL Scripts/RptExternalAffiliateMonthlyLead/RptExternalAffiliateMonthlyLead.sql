USE [DataMartV2]
GO
	
--------------------- Drop SP -------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reports].[RptExternalAffiliateMonthlyLead]') AND OBJECTPROPERTY(OBJECT_ID, N'IsProcedure') = 1)
DROP PROCEDURE [Reports].[RptExternalAffiliateMonthlyLead]
GO


CREATE PROCEDURE [Reports].[RptExternalAffiliateMonthlyLead]
@Year NVARCHAR(4),
@Month NVARCHAR(10),
@Product NVARCHAR(MAX),
@TenantID UNIQUEIDENTIFIER,
@AffiliateGUID NVARCHAR(MAX)

AS BEGIN

DECLARE @StartDate DATE,
		@EndDate DATE


SET @StartDate = CAST(CAST(@Year AS VARCHAR(4)) + RIGHT('0' + CAST(@Month AS VARCHAR(2)), 2) + RIGHT('0' + CAST(01 AS VARCHAR(2)), 2) AS DATE)
SET @EndDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@StartDate)+1,0))
SET @StartDate = DATEADD(m,-5,@StartDate) -- Set the start date to 6 months prior to the month selected


----------------- Insert Leads Received ---------------------
SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffiliateShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   COUNT(DISTINCT dld.LeadId) AS LeadsReceived,
	   0 AS LeadsAccepted
INTO 
	   #ExternalAffiliateMonthlyLead
FROM 
	   LeadDetail dld WITH (NOLOCK)
INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
WHERE 
	   CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
	   AND dld.IsReceived = 1
GROUP BY
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.Name,
	   dp.ProductId



--------------- Set Leads Accepted -------------------
;WITH CTE AS
(
 SELECT
	   COUNT(DISTINCT dld.LeadId) AS Cnt, 
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS ShortCode,
	   dld.ProductId
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 WHERE 
	   dld.IsAccepted = 1
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dld.ProductId
) 
UPDATE 
	  #ExternalAffiliateMonthlyLead
SET 
	  LeadsAccepted = CTE.Cnt
FROM 
	  #ExternalAffiliateMonthlyLead eaml WITH (NOLOCK)
INNER JOIN 
	  CTE WITH (NOLOCK)
	  ON eaml.LeadYear = CTE.LeadYear
	  AND eaml.LeadMonth = CTE.LeadMonth
	  AND eaml.AffiliateShortCode = CTE.ShortCode
	  AND eaml.ProductId = CTE.ProductId


--------------- Insert Leads Accepted -------------------
INSERT INTO 
	   #ExternalAffiliateMonthlyLead
 SELECT DISTINCT
	   DATENAME(YEAR, dld.[Date]) AS LeadYear,
	   DATENAME(MONTH, dld.[Date]) AS LeadMonth,
	   dasc.Code AS AffilaiteShortCode,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name AS Product,
	   0 AS LeadsReceived,
	   COUNT(DISTINCT dld.LeadId) AS LeadsAccepted
 FROM 
	   LeadDetail dld WITH (NOLOCK)
 INNER JOIN 
	   Lead dl WITH (NOLOCK)
	   ON dld.LeadId = dl.LeadId
	   AND dl.IsTest = 0
 INNER JOIN 
	   Product dp WITH (NOLOCK)
	   ON dld.ProductId = dp.ProductId
 INNER JOIN 
	   Affiliate da WITH (NOLOCK)
	   ON da.AffiliateId = dld.AffiliateId
 INNER JOIN 
	   AffiliateShortCode dasc WITH (NOLOCK)
	   ON dasc.AffiliateShortCodeId = dld.AffiliateShortCodeId
 INNER JOIN
       Tenant dt WITH (NOLOCK)
	   ON dt.TenantId = dld.TenantId
 INNER JOIN 
	   Campaign dcam WITH (NOLOCK)
	   ON dcam.CampaignId = dld.CampaignId
 LEFT OUTER JOIN 
	   CampaignRule dcr WITH (NOLOCK)
	   ON dcr.CampaignRuleId = dld.CampaignRuleId
 LEFT OUTER JOIN
	   #ExternalAffiliateMonthlyLead eaml WITH (NOLOCK)
	   ON eaml.LeadYear = DATENAME(YEAR, dld.[Date])
	   AND eaml.LeadMonth = DATENAME(MONTH, dld.[Date])
	   AND eaml.AffiliateShortCode = dasc.Code
	   AND eaml.ProductId = dp.ProductId
 WHERE 
	   dld.IsAccepted = 1
	   AND eaml.LeadsAccepted IS NULL
	   AND CONVERT(Date,dld.[Date]) >= @StartDate AND CONVERT(Date,dld.[Date]) <= @EndDate
	   AND da.OldAffiliateId IN (SELECT Value FROM dbo.FnSplit(@AffiliateGUID,','))
	   AND dld.ProductId IN (SELECT Value FROM dbo.FnSplit(@Product,','))    
	   AND dt.OldTenantId = @TenantID
	   AND dld.IsValid = 1
 GROUP BY 
	   DATENAME(YEAR, dld.[Date]),
	   DATENAME(MONTH, dld.[Date]),
	   dasc.Code,
	   dasc.[Description],
	   dp.ProductId,
	   dp.Name


--------------- Select records for the report -------------------
SELECT DISTINCT 
	   LeadYear,
	   LeadMonth,
	   AffiliateShortCode,
	   [Description],
	   Product,
	   LeadsReceived,
	   LeadsAccepted
FROM 
	  #ExternalAffiliateMonthlyLead WITH (NOLOCK)

RETURN

---------- Drop #ExternalAffiliateMonthlyLead Temp table -------------------
IF  OBJECT_ID (N'tempdb..#ExternalAffiliateMonthlyLead') IS NOT NULL
DROP TABLE #ExternalAffiliateMonthlyLead


END